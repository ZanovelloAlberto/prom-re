{ pkgs ? import <nixpkgs> { } }:
pkgs.stdenv.mkDerivation {
  name = "caio";
  buildInputs = with pkgs;[
    opentofu
    # grpc-tools
    # protoc-gen-go
    # protoc-gen-go-grpc
    # protobuf
    # go
    # ngrok
    mongosh
    # mongodb-tools
  ];
  dontUnpack = true;

  shellHook = ''
    updateShared(){
      rm -r app/src/shared-api
      cp -r shared-api app/src
    }
    deploy() {
      tofu apply -auto-approve
      ngrok http http://localhost:5000
    }

    symlink() {
      cd http-api/node_modules
      ln -s ../../prometeo-api .

      cd ../../  
      cd http-api/node_modules
      ln -s ../../prometeo-api .
      cd ../../  

    }
    protogen() {
      protoc --go_out=./server --go_opt=paths=source_relative \
      --go_out=./http-api --go_opt=paths=source_relative \
      --go-grpc_out=./server --go-grpc_opt=paths=source_relative \
      --go-grpc_out=./http-api --go-grpc_opt=paths=source_relative \
      server-proto/main.proto    
    }
  '';

}
