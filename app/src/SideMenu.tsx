import * as Icons from "@mui/icons-material"
import { Container, Icon, List, ListItem, ListItemButton, ListItemIcon, ListItemText } from "@mui/material"
import { useNavigate } from "react-router-dom"

export interface menuItem {
    name: string
    path?: string
    elmentIcon?: JSX.Element
    children?: menuItem[]
}

const mod = {

    menu: [
        {
            name: 'Generale',
            elmentIcon : <Icons.Home/>,
            children: [
                {
                    name: "Urgenze",
                    path: "urgency",
                    elmentIcon: <Icons.HourglassBottom/>
                },
                {
                    name: "User",
                    path: "user",
                    elmentIcon: <Icons.Person/>
                },
                {
                    name: "Notifiche",
                    path: "notification",
                    elmentIcon: <Icons.Mail/>
                },
                {
                    name: "Dashboard",
                    path: "dashboard",
                    elmentIcon: <Icons.Dashboard/>,
                },

                // {
                //     name: "Rubrica"
                // },
                // {
                //     name: "Commensse"
                // },
                // {
                //     name: "Reparti"
                // },
                {
                    name: "Aziende"
                },
                {
                    name: "Configurazione",
                    children: [
                        {
                            name: "Titoli dei contatti"
                        },
                        {
                            name: "Tipi di comunicazione"
                        },
                        {
                            name: "Tipi di mezzi di comunicazione"
                        }
                    ]
                },
            ]
        },
        // {
        //     name: "Comunicazioni",
        //     children: [
        //         {
        //             name: "Posta",
        //             children: [
        //                 {
        //                     name: "Nuova Mail"
        //                 }
        //             ]
        //         }
        //     ]
        // },
        {
            name: "Attivita",
            children: [
                {
                    name: "Calendario"
                },
                {
                    name: "Rilevazioni",
                    children: [
                        {
                            name: "Nuova rilevazione"
                        }
                    ]
                },
                // {
                //     name: "Attivita pianificate",
                //     children: [
                //         {
                //             name: "Nuova attivita pianificata"
                //         }
                //     ]
                // },
                // {
                //     name: "Oridini di lavoro",
                //     children: [
                //         {
                //             name: "Nuovo ordine di lavoro"
                //         }
                //     ]
                // },
            ]
        }
    ]

} as {
    menu: menuItem[]
}

interface MenuProps {

}


export default (props: MenuProps) => {
    const navigate = useNavigate()
   
    const rederListTreeRec = (is: menuItem[]) => {
        return (
          <List disablePadding>
            {is.map((v, i) => {
              return (
                <>
                  <ListItem key={i} sx={{ size: "small" }}>
                    <ListItemButton onClick={()=>{
                        if(v.path){
                            navigate(v.path)
                        }
                    }}>
                      <ListItemIcon>
                        {/* metti ingranaggio */}
                        {v.elmentIcon ? v.elmentIcon :  (<Icons.Settings/> )}
                      </ListItemIcon>
                      <ListItemText primary={v.name} />
                    </ListItemButton>
                  </ListItem>
                  <Container key={i + v.name}>
                    {v.children ? rederListTreeRec(v.children) : null}
                  </Container>
                </>)
            })}
          </List>
        )
      }
    // made this not recursive

    
      return  rederListTreeRec(mod.menu)

}

export const MenuFlat = ({}: MenuProps)  => {
    const navigate = useNavigate()
    return (
        <List>
            {sideMneuFlat.map((v, i) => {
                return (
                    <ListItem key={i} sx={{ size: "small" }}>
                        <ListItemButton onClick={()=>{
                            if(v.path){
                                navigate(v.path)
                            }
                        }}>
                            <ListItemIcon>
                                {v.elmentIcon}
                            </ListItemIcon>
                            <ListItemText primary={v.name} />
                        </ListItemButton>
                    </ListItem>
                )
            })}
        </List>
    )
}

export const sideMneuFlat = [
    {
        name: "Urgenze",
        path: "urgency",
        elmentIcon: <Icons.HourglassBottom/>
    },
    {
        name: "User",
        path: "user",
        elmentIcon: <Icons.Person/>
    },
    {
        name: "Notifiche",
        path: "notification",
        elmentIcon: <Icons.Mail/>
    },
    {
        name: "Dashboard",
        path: "dashboard",
        elmentIcon: <Icons.Dashboard/>,
    },
    {
        name: "Aziende",
        path: "company",
        elmentIcon: <Icons.Business/>
    },
    {
        name : "Contatti",
        path: "contact",
        elmentIcon: <Icons.ContactPhone/>
    }
]