import axios, { CreateAxiosDefaults } from "axios";
import { useContext } from "react";
import { useNavigate, useNavigation } from "react-router-dom";

export const baseConfig: CreateAxiosDefaults<any> = {
    baseURL: "http://localhost:5000/",
    // headers: {
    //     'Content-Type': 'application/json',
    //     'Access-Control-Allow-Origin:' :'*'
    // },
}

// export const authAxio = axios.create({
//     ...baseConfig,
//     transformRequest: (data, headers) => {

//         // just change the authorization header
//         headers.Authorization = session?.token;
//         headers['Content-Type'] = 'application/json';
//         return JSON.stringify(data);

//      },
    
//     transformResponse: (data) => {
//         // let {sidebarEnable}  = useContext(AuthContext)
//         // console.log(sidebarEnable)
//         return data;
//     },
    
    
// })


export const publicAxio = axios.create({
    ...baseConfig
});