import { Box, Container, MenuItem, Stack, TextField, Typography } from "@mui/material";
import { Dictionary, Language, LanguageContext, LanguageSelector } from "Contexts/Language";
import { useContext } from "react";
import { Link } from "react-router-dom";

const LandingPage = () => {
  let { getLang} = useContext(LanguageContext)
  return (
    <Box
      sx={{
        padding: 2,
       }}
    >
      <Container>
        <Stack spacing={2}
          direction={"row"}
        ><Box
            sx={{ flexGrow: 1 }}
          />
          <LanguageSelector />
        </Stack>
      </Container>
      <Box
        // set center vertically and horizontally
        // getting al the space available
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '90vh',
        }}
      >
        <Stack spacing={2} textAlign="center">
          <Typography variant="h5" component="h2" gutterBottom>
          {getLang(Dictionary.YourContentGoesHere)}
          </Typography>
          <Typography variant="body1">
            This is a centered content example using Material-UI v5.
          </Typography>
          <Typography component={Link} to={"dashboard"} variant="body1">
            dashboard
          </Typography>
        </Stack>
      </Box>
    </Box>)
}


export default LandingPage;