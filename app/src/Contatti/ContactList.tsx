import { Add, CheckBox, Circle, Clear, Delete, Edit, Label, QueryBuilder, Search } from "@mui/icons-material"
import { TextField, Stack, Grid, Button, TableCell, TableRow, TableBody, TableHead, Paper, colors, TableContainer, Table, TablePagination, Icon, IconButton, Box, Select, MenuItem, Container, Checkbox, FormControlLabel, } from "@mui/material"
import { AuthContext } from "Contexts/Auth"
import { Dictionary, LanguageContext } from "Contexts/Language"
import Pagination from "Pagination/PaginationBase"
import { HttpStatusCode } from "axios"
import { loadavg } from "os"
import { ContactItem, ContactListItem, ContactApiDescription, ContactFilterItem } from "common/src/configuration/contact.js"
import { DeleteListId, ListResponse } from "common/src/common"
import { useContext, useEffect, useState } from "react"
import { Link, useLocation, useNavigate, useSearchParams } from "react-router-dom"

enum formNames {
    NAME = "name",
    DESCR = "descr",
    VALUES = "values"
}
interface Filter {
    name: string,
    descr: string,
    values: []
}

export default () => {

    const [searchParams, setSearchParams] = useSearchParams();
    const location = useLocation()
    const navigate = useNavigate()
    let { showBar, authAxio } = useContext(AuthContext)
    let { getLang } = useContext(LanguageContext)

    const ps = location?.state?.pageSize ?? 10
    const pn = location?.state?.pageNumber ?? 0
    const values = location?.state?.values ?? []
    const isSameCounter = location?.state?.isSameCounter ?? 0

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState<ListResponse<ContactListItem>>({ totalRecords: 0, list: [], page: 0 })

    const [search, setSearch] = useState<Filter>({
        name: "",
        descr: "",
        values: []
    })

    const Call = (sp: URLSearchParams) => {
        let newSearch: ContactFilterItem = {
            name: sp.get(formNames.NAME) ?? "",
            descr: sp.get(formNames.DESCR) ?? "",
            pageNumber: pn,
            pageSize: ps
        };

        authAxio.post(ContactApiDescription.getListContacts.path, newSearch).then(x => {
            if (x.status === HttpStatusCode.Ok) {
                setData(x.data)
            }
            setIsLoading(false)
        })
    }

    useEffect(() => {
        setSearch({
            descr: searchParams.get(formNames.DESCR) ?? "",
            name: searchParams.get(formNames.NAME) ?? "",
            values: values ?? []
        })
        Call(searchParams)
    }, [pn, ps, searchParams, isSameCounter])


    return (
        <Pagination
            isLoading={isLoading}
            title={getLang(Dictionary.Contacts)}
            other={<Link to={"new"}><Button variant='contained' fullWidth endIcon={<Add />} >add new</Button></Link>}
        >
            <Box marginTop={1} marginBottom={1}>
                {/* // create a filter for the list */}
                <form onSubmit={(e: any) => {
                    e.preventDefault();

                    let values = {
                        [formNames.DESCR]: e.target[formNames.DESCR].value,
                        [formNames.NAME]: e.target[formNames.NAME].value,
                    }

                    let newSearch = new URLSearchParams()
                    values[formNames.DESCR] && newSearch.append(formNames.DESCR, values[formNames.DESCR])
                    values[formNames.NAME] && newSearch.append(formNames.NAME, values[formNames.NAME])

                    let isSame = newSearch.toString() == searchParams.toString()

                    setIsLoading(true)
                    setSearchParams(newSearch, { state: { pageNumber: 0, pageSize: ps, values: [], isSameCounter: (isSameCounter + 1) }, replace: (isSame) })
                }}>
                    <Grid container spacing={2}>

                        {/* FILTER FIELDS */}
                        <Grid item xs={4}>
                            <TextField
                                size='small'
                                variant="standard"
                                value={search[formNames.NAME]}
                                onChange={(e) => setSearch({ ...search, [formNames.NAME]: e.target.value })}
                                fullWidth
                                name={formNames.NAME}
                                label={getLang(Dictionary.Name)}
                            />

                        </Grid>
                        <Grid item xs={4}>

                            <TextField
                                size='small'
                                variant="standard"
                                name={formNames.DESCR}
                                value={search[formNames.DESCR]}
                                onChange={(e) => setSearch({ ...search, [formNames.DESCR]: e.target.value })}
                                fullWidth
                                label={getLang(Dictionary.Description)}
                            />
                        </Grid>
                        <Grid item xs/>


                        <Grid item xs={6} />
                        <Grid item xs={3}>
                            <Button
                                variant="outlined"
                                fullWidth
                                type='submit'
                                endIcon={<Search />}
                            >
                                {getLang(Dictionary.Search)}
                            </Button>
                        </Grid>
                        <Grid item xs={3}>
                            <Button
                                variant="outlined"
                                fullWidth
                                onClick={() => {
                                    // setSearchParams({})
                                    setSearch({ name: "", descr: "", values: [] })
                                }}
                                endIcon={<Clear />}
                            >
                                {getLang(Dictionary.ClearFilter)}
                            </Button>
                        </Grid>

                    </Grid>
                </form>
            </Box>
            <Grid item xs flexDirection={"column"}>
                <TableContainer component={Paper} sx={{ backgroundColor: colors.grey[300] }}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>

                                <TableCell>{getLang(Dictionary.Name)}</TableCell>
                                <TableCell>{getLang(Dictionary.Description)}</TableCell>
                                <TableCell align="right">{getLang(Dictionary.Actions)}</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.list.map((item: ContactListItem, i) => {
                                console.log(item);
                                
                                return (
                                    <TableRow key={item.id}>
                                        <TableCell>{item.name}</TableCell>
                                        <TableCell>{item.descr}</TableCell>
                                        <TableCell align='right'>

                                            <IconButton onClick={() => { navigate(`edit/${item.id}`) }}><Icon sx={{ color: "blue" }} ><Edit /></Icon></IconButton>
                                            <IconButton onClick={() => {

                                                let value: DeleteListId = {
                                                    ids: [item.id]
                                                }
                                                setIsLoading(true)

                                                authAxio.post(ContactApiDescription.deleteContacts.path, value).then(x => {
                                                    if (x.status === HttpStatusCode.Ok) {
                                                        showBar(`${getLang(Dictionary.Contact)} ${getLang(Dictionary.Deleted)}`, "success")
                                                        navigate("", {
                                                            state: { pageNumber: 0, pageSize: ps },
                                                        })
                                                    }
                                                    setSearchParams(searchParams, { state: { pageNumber: 0, pageSize: ps, isSameCounter: (isSameCounter + 1) }, replace: true })
                                                })
                                            }}>

                                                <Icon ><Delete /></Icon>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 50]}
                    component="div"
                    count={data.totalRecords}
                    rowsPerPage={ps}
                    labelRowsPerPage={getLang(Dictionary.RowsPerPage)}
                    page={pn}
                    onPageChange={(a, page) => {
                        navigate("", {
                            state: { pageNumber: page, pageSize: ps },
                        })
                    }}
                    onRowsPerPageChange={(e) => {
                        navigate("", {
                            state: { pageNumber: 0, pageSize: e.target.value },
                        })
                    }}
                />
            </Grid>
        </Pagination>
    )
}
