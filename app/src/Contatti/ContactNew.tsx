import { useContext, useEffect, useState } from "react";
import Pagination from "../Pagination/PaginationBase";
import { Formik } from "formik";
import { Form, useNavigate, useParams } from "react-router-dom";
import { Button, CircularProgress, Grid, MenuItem, Paper, TextField, Typography, colors } from "@mui/material";
import { ContactItem, ContactApiDescription, validateContact, ContactValue } from "common/src/configuration/contact";
import { HttpStatuses, ItemWrapper } from "common/src/common";
import { AuthContext } from "Contexts/Auth";
import { Dictionary, LanguageContext } from "Contexts/Language";
import BaseTextField from "Components/BaseTextField";

enum formName {
    NAME = "name",
    DESCR = "descr",
    VALUES = "values"
}
const defaultContactItem: ContactItem = {
    id: "",
    name: "",
    descr: "",
    values: [],
    companyId: ""
}

export default () => {
    let navigate = useNavigate()
    const { getLang } = useContext(LanguageContext)
    let [isLoading, setIsLoading] = useState(false)
    useEffect(() => {
    }, [])
    let { authAxio } = useContext(AuthContext)

    return (<Pagination title={`${getLang(Dictionary.New)} ${getLang(Dictionary.Contact)}`} isLoading={isLoading}>
        <Formik
            initialValues={defaultContactItem}
            validationSchema={validateContact}
            onSubmit={(values) => {
                let data = {
                    item: values
                } as ItemWrapper<ContactItem>;
                setIsLoading(true)
                authAxio.post(`${ContactApiDescription.postContact.path}`, data).then(x => {
                    if(x.status === HttpStatuses.Created){
                        navigate("./..")
                    }           
                    setIsLoading(false)      
                })
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => (
                <Paper sx={{ padding: 2, backgroundColor: colors.grey[200] }} >
                    <Grid container component={Form} method="post" replace onSubmit={handleSubmit} spacing={3}>
                    <Grid item xs={6}>

                            <BaseTextField
                                formik={{ values, errors, handleChange }}
                                name={formName.NAME}
                                props={{
                                    type: "password"
                                }}
                                label={getLang(Dictionary.Name)} />
                        </Grid>
                        <Grid item xs={6}>
                            <BaseTextField
                                formik={{ values, errors, handleChange }}
                                name={formName.DESCR}
                                label={getLang(Dictionary.Description)} />
                        </Grid>

                        <Grid item xs={12}>
                            <Button
                                sx={{ float: "right" }}
                                type="submit"
                                variant="contained">
                                {getLang(Dictionary.Save)}
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
            )}</Formik>
    </Pagination>)
}