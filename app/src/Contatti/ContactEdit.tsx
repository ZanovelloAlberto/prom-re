import { useContext, useEffect, useState } from "react";
import Pagination from "../Pagination/PaginationBase";
import { Formik } from "formik";
import { Form, useNavigate, useParams } from "react-router-dom";
import { Button, Grid, MenuItem, Paper, TextField, Typography, colors } from "@mui/material";
import {  ContactItem, ContactApiDescription, validateContact } from "common/src/configuration/contact";
import { ItemWrapper } from "common/src/common";
import { AuthContext } from "Contexts/Auth";
import { Dictionary, LanguageContext } from "Contexts/Language";
import BaseTextField from "Components/BaseTextField";
enum formName {
    NAME = "name",
    DESCR = "descr",
    VALUES = "values"
}

const defaultContactItem: ContactItem = {
    id: "",
    name: "",
    descr: "",
    values: [],
    
}

export default () => {

    let { getLang } = useContext(LanguageContext)
    let navigate = useNavigate()
    let { showBar, authAxio } = useContext(AuthContext)

    let params = useParams()
    let [isLoading, setIsLoading] = useState(true)
    let [data, setData] = useState<ContactItem>({
        name: "",
        id: "",
        descr: "",
        values: [],

    })
    useEffect(() => {
        authAxio.get(`${ContactApiDescription.getSingeContact.path}/${params.id}`).then(x => {

            let data : ContactItem = x.data;

            setData({
                ...data                

            } as ContactItem)
            setIsLoading(false)
        })
    }, [])

    return (<Pagination title={`${getLang(Dictionary.Edit)} ${getLang(Dictionary.Contact)}`} isLoading={isLoading}>
        <Formik
            enableReinitialize
            initialValues={data}
            validationSchema={validateContact}
            onSubmit={(values) => {
                let data = {
                    id: params.id,
                    item: values
                } as ItemWrapper<ContactItem>;
                console.log(values);


                authAxio.post(`${ContactApiDescription.postContact.path}`, data).then(x => {
                    navigate("./../..")
                })
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => {
                return (
                    <Paper sx={{ padding: 4, backgroundColor: colors.grey[200] }} >
                        <Grid container component={Form} method="post" replace onSubmit={handleSubmit} spacing={3}>


                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.NAME}
                                    label={getLang(Dictionary.Name)} />
                            </Grid>
                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.DESCR}
                                    label={getLang(Dictionary.Description)} />
                            </Grid>
                            <Grid item xs={6}>
                                {/* <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.PERMISSIONS}
                                    label={getLang(Dictionary.Permissions)}
                                    props={{
                                        select: true,
                                        SelectProps: {
                                            multiple: true,
                                        }
                                    }}
                                >
                                    {Object.keys(DefaultPermissions).map((x) => (
                                        <MenuItem key={x} value={x}>
                                            {x}
                                        </MenuItem>
                                    ))}
                                </BaseTextField> */}
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    sx={{ float: "right" }}
                                    type="submit"
                                    variant="contained">
                                    {getLang(Dictionary.Save)}
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>

                )
            }}</Formik>
    </Pagination>)
}