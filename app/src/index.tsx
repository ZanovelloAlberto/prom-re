import { Button, ThemeProvider, createTheme } from '@mui/material';
import Authenticate from 'AuthLayout';
import Dashboard from 'Dashboard';
import LandingPage from 'LandingPage';
import LoginPage from 'LoginPage';
import Test from 'Test';
import UrgencyEdit from 'Urgenze/UrgencyEdit';
import UrgencyList from 'Urgenze/UrgencyList';
import UrgencyNew from 'Urgenze/UrgencyNew';
import React, { Suspense, lazy, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import { IndexRouteObject, LoaderFunctionArgs, Navigate, RouteObject, RouterProvider, createBrowserRouter, useLocation, useNavigate } from 'react-router-dom';
import { AuthChecker, AuthContextProvider } from "Contexts/Auth";
import Notificate from 'Notification/Notificate';
import { LanguageContextProvider } from 'Contexts/Language';
import { publicAxio } from 'Api';
export const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  palette: {
    primary: {
      main: '#1976d2',
    },
    secondary: {
      main: '#dc004e',
    },
  },
})

const routes: RouteObject[] = [
  {
    path: "/",
    
    Component: LandingPage,
    ErrorBoundary: () => {
      return <h1>error</h1>
    }
  },
  {
    Component: AuthChecker,
    children: [
      {
        path: "dashboard",
        Component: Dashboard
      },
      {
        path: "urgency",
        Component: UrgencyList
      },
      {
        path: "urgency/edit/:id",
        Component: UrgencyEdit
      },
      {
        path: "urgency/new/",
        Component: UrgencyNew
      },
      {
        path: "user",
        Component: lazy(() => import('Utenti/UserList'))
      },
      {
        path: "user/edit/:id",
        Component: lazy(() => import('Utenti/UserEdit'))
      },
      {
        path: "user/new",
        Component: lazy(() => import('Utenti/UserNew'))
      },
      {
        path: "contact",
        Component: lazy(() => import('Contatti/ContactList'))
      },
      {
        path: "contact/edit/:id",
        Component: lazy(() => import('Contatti/ContactEdit'))
      },
      {
        path: "contact/new",
        Component: lazy(() => import('Contatti/ContactNew'))
      },
      {
        path: "notification",
        Component: Notificate
      }
    ]

  },
  {
    path: "login",
    Component: LoginPage
  },
  // {
    
    // path: "auth/emailVerification/:code",
    // element: <Navigate to="localhost:5000/" />,
  // }
  // {
  //   path: "auth/emailVerification/:code",
  //   Component: ()=>{
  //     const [data, setData] = React.useState<any>("loading...")
  //     let {pathname} = useLocation()
  //     useEffect(() => {
  //     publicAxio.get(pathname)
  //     },[])
  //     return <>
  //       {data}
  //     </>

  //   }
  // },
  {
    path: "test",
    element:
      <Test />
    // Component: Test
  }
]


const router = createBrowserRouter(routes);
const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <LanguageContextProvider>
    <AuthContextProvider>
      <Suspense fallback={<div>Loading...</div>}>
        <RouterProvider router={router} />
      </Suspense>
    </AuthContextProvider>
  </LanguageContextProvider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
