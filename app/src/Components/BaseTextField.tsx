import { TextField, TextFieldProps, Typography } from '@mui/material';
import { useContext } from 'react';
interface IBaseTextField {
    formik: {
        values: any;
        errors: any;
        handleChange: any;
    }
    label: string;
    name: string;
    props?: TextFieldProps;
    children?: any;
}

export default ({ name, formik, label, props,children }: IBaseTextField) => {
    return (
        <>
            <Typography>{label}</Typography>
            <TextField
                fullWidth
                size='small'
                error={formik.errors[name] == undefined ? false : true}
                name={name}
                value={formik.values[name]}
                onChange={formik.handleChange}
                helperText={formik.errors[name]}
                variant="outlined"
                {...props}
            >{children}</TextField>
        </>)
}