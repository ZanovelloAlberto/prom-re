import { Button } from "@mui/material"
import { AuthContext } from "Contexts/Auth"
import axios from "axios"
import { notificationAPIDescription } from "common/src/notification"
import { useContext } from "react"



export default () => {
    let {showBar,authAxio} = useContext(AuthContext)
    return (
        <>
            <Button onClick={() => {
                authAxio.post(notificationAPIDescription.notifyMe.path,{text : "ciao"}).then(x => {
                    console.log(x);
                    showBar("Email sent","success")
                }).catch(x => {

                });
            }}>Send Email</Button>
        </>
    )
}