// App.js

import React from 'react';
import { Drawer, Hidden, AppBar, Toolbar, IconButton, Typography, Button, createTheme, ThemeProvider, makeStyles, Box } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { useState } from 'react';
import { Theme } from '@emotion/react';
import { theme } from 'index';

const drawerWidth = 240;



// const useStyles = makeStyles((theme: any) => ({
//   root: {
//     display: 'flex',
//   },

//   appBar: {
//     [theme.breakpoints.up('md')]: {
//       width: `calc(100% - ${drawerWidth}px)`,
//       marginLeft: drawerWidth,
//     },
//   },
//   menuButton: {
//     marginRight: theme.spacing(2),
//     [theme.breakpoints.up('md')]: {
//       display: 'none',
//     },
//   },
//   // necessary for content to be below app bar
//   toolbar: theme.mixins.toolbar,
//   drawerPaper: {
//     width: drawerWidth,
//   },
//   content: {
//     flexGrow: 1,
//     padding: theme.spacing(3),
//   },
// }));

function App() {


  const [mobileOpen, setMobileOpen] = useState(false);


  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };


  return (
    <ThemeProvider theme={theme}>
      <div >
        <AppBar position="fixed"
          sx={{
            [theme.breakpoints.up('md')]: {
              width: `calc(100% - ${drawerWidth}px)`,
              marginLeft: drawerWidth,
            }
          }}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              sx={{
                marginRight: theme.spacing(2),
                [theme.breakpoints.up('md')]: {
                  display: 'none',
                },
              }}            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap sx={{ flexGrow: 1 }}>
              Responsive Sidebar
            </Typography>
            <Button color="inherit">Logout</Button>
          </Toolbar>
        </AppBar>
        <nav aria-label="mailbox folders">
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Hidden mdUp implementation="css">
            <Drawer
              variant="temporary"
              anchor={'right'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
    
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
            >
            </Drawer>
          </Hidden>
          <Hidden smDown implementation="css">
            <Drawer
              classes={{
              }}
              variant="permanent"
              open
            >
            </Drawer>
          </Hidden>
        </nav>
        <main >
          <div  />
          {/* Add your main content here */}
          <Typography paragraph>
            Main Content Here
          </Typography>
        </main>
      </div>
    </ThemeProvider>
  );
}

export default App;