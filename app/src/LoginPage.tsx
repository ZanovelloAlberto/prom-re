import { Box, Button, Checkbox, Container, FormControlLabel, Grid, Link, Stack, TextField, Typography } from "@mui/material";
import { publicAxio } from "Api";
import { Formik } from "formik";
import { Form, useNavigate, useParams } from "react-router-dom";
import * as yup from 'yup';
import { AuthLoginValidation } from "common/src/login";
import { useContext } from "react";
import { AuthContext } from "Contexts/Auth";

export default () => {
    let navigate = useNavigate();
    let { Login } = useContext(AuthContext)
    return (<Container component="main" maxWidth="xs">
        <Box
            sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
        >
            <Typography component="h1" variant="h4" sx={{ color: "blue" }}>
                Promenteo
            </Typography>
            <br />
            <Typography component="h2" variant="h5">
                Sign in
            </Typography>
            <Box sx={{ mt: 1 }}>
                <Formik
                    initialValues={{ email: "", password: "" }}
                    validationSchema={AuthLoginValidation}
                    onSubmit={ (values) => {
                        
                        Login(values.email, values.password).then((x) => {
                            if(x){
                                navigate("/dashboard")
                            }else{
                                alert("Login failed")
                            }
                        })
                    }}
                >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                        <Box component={Form} method="post" replace onSubmit={handleSubmit} sx={{ mt: 1 }}>

                            <TextField
                                margin="normal"
                                fullWidth
                                name="email"
                                label="Email Address"
                                error={errors.email != undefined}
                                autoComplete="email"
                                onChange={handleChange}
                                value={values.email}
                                helperText={errors.email}
                            />
                            <TextField
                                margin="normal"
                                fullWidth
                                name="password"
                                label="Password"
                                error={errors.password != undefined}
                                helperText={errors.password}
                                onChange={handleChange}
                                value={values.password}
                                autoComplete="current-password"
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                            >
                                Login
                            </Button>
                        </Box>
                    )}</Formik>

                {/* <Grid container>
                    <Grid item xs>
                        <Link href="#" variant="body2">
                            Forgot password?
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link href="#" variant="body2">
                            {"Don't have an account? Sign Up"}
                        </Link>
                    </Grid>
                </Grid> */}
            </Box>
        </Box>
    </Container>)
}

