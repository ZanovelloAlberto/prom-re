import {
    TableRow, TableCell,
    TableHead, TableContainer, Table,
    TableBody, Paper, TablePagination, IconButton, TextField, Container, Typography, Stack, Box, Button, Grid, colors, CircularProgress
} from '@mui/material';

interface paginationProps {
    children: any
    title: string
    other?: any
    isLoading? : boolean
}

export default  ({ children, title, other, isLoading }: paginationProps) => {
    // awati a timer of 2 sec
    return (
        <Container maxWidth="md" sx={{ marginTop: 3 }}>
            {isLoading && <div style={{ position: 'fixed', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>
                <CircularProgress />
            </div>}
            {<Grid container spacing={2}>
                <Grid item xs={8}>
                    <Box sx={{ padding: 1 }}>
                        <Typography variant="h3" component="h1" gutterBottom>
                            {title}
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={4} >
                    <Box sx={{ paddingTop: 3, paddingLeft: 4 }}>
                        {other}
                    </Box>
                </Grid>
                <Grid item xs={12} >
                    {children}
                </Grid>
            </Grid>}
        </Container>)
}