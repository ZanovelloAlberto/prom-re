import { useContext, useEffect, useState } from "react";
import Pagination from "../Pagination/PaginationBase";
import { Formik } from "formik";
import { Form, useNavigate, useParams } from "react-router-dom";
import { Button, Grid, MenuItem, Paper, TextField, Typography, colors } from "@mui/material";
import { PriorityType, UrgencyItem, urgencyApiDescription, validateUrgencyItem } from "common/src/configuration/urgency";
import { DeleteListId, HttpStatuses, ItemWrapper, ListResponse } from "common/src/common"
import { AuthContext } from "Contexts/Auth";
import { Dictionary, LanguageContext } from "Contexts/Language";
import BaseTextField from "Components/BaseTextField";

const formName = {
    CODE: "code",
    DESCR: "descr",
    COLOR: "color",
    PRIORITY: "priority"
}
export default () => {

    let { getLang } = useContext(LanguageContext)
    let navigate = useNavigate()
    let { showBar, authAxio } = useContext(AuthContext)

    let params = useParams()
    let [isLoading, setIsLoading] = useState(true)
    let [data, setData] = useState<UrgencyItem>({
        code: "",
        descr: "",
        color: "#000000",
        priority: PriorityType.Low
    })
    useEffect(() => {
        // url.searchParams.append("id", params.id)
        authAxio.get(`${urgencyApiDescription.getSingeUrgency.path}/${params.id}`).then(x => {
            let data = x.data;
            console.log(data);

            setData({
                code: data.code,
                descr: data.descr,
                color: data.color,
                priority: data.priority
            } as UrgencyItem)
            setIsLoading(false)
        }).catch((out) => {
            console.log(out);
        })
    }, [])

    return (<Pagination title="Edit urgency" isLoading={isLoading}>
        <Formik
            enableReinitialize
            initialValues={data}
            validationSchema={validateUrgencyItem}
            onSubmit={(values) => {
                let data = {
                    id: params.id,
                    item: values
                } as ItemWrapper<UrgencyItem>;
                console.log(values);


                authAxio.post(`${urgencyApiDescription.postUrgency.path}`, data).then(x => {
                    if(x.status !== HttpStatuses.Accepted){
                        navigate("./../..")
                    }
                })
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => {
                return (
                    <Paper sx={{ padding: 4, backgroundColor: colors.grey[200] }} >
                        <Grid container component={Form} method="post" replace onSubmit={handleSubmit} spacing={3}>

                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.CODE}
                                    label={getLang(Dictionary.Code)} />
                            </Grid>
                            <Grid item xs={6} />
                            <Grid item xs={12}>
                            <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.DESCR}
                                    label={getLang(Dictionary.Description)} />
                            </Grid>
                            <Grid item xs={6}>
                            <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.PRIORITY}
                                    props={{
                                        select: true
                                    }}
                                    label={getLang(Dictionary.Priority)}>
                                    <MenuItem value={PriorityType.Low}>Low</MenuItem>
                                    <MenuItem value={PriorityType.High}>High</MenuItem>
                                    <MenuItem value={PriorityType.Mid}>Mid</MenuItem>
                                    {/* <MenuItem value={""}><em>{getLang(Dictionary.None)}</em></MenuItem> */}
                                    </BaseTextField>
                            </Grid>
                            <Grid item xs={6}>
                            <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.COLOR}
                                    props={{
                                        type: "color"
                                    }}
                                    label={getLang(Dictionary.Color)} />
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    sx={{ float: "right" }}
                                    type="submit"
                                    variant="contained">
                                    {getLang(Dictionary.Save)}
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>

                )
            }}</Formik>
    </Pagination>)
}