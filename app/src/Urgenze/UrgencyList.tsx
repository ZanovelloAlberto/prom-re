import { Add, Circle, Clear, Delete, Edit, Label, QueryBuilder, Search } from "@mui/icons-material"
import { TextField, Stack, Grid, Button, TableCell, TableRow, TableBody, TableHead, Paper, colors, TableContainer, Table, TablePagination, Icon, IconButton, Box, Select, MenuItem, Container, } from "@mui/material"
import { AuthContext } from "Contexts/Auth"
import { Dictionary, LanguageContext } from "Contexts/Language"
import Pagination from "Pagination/PaginationBase"
import { loadavg } from "os"
import { PriorityType, UregencyFilterItem, UrgencyItem, UrgencyListItem, urgencyApiDescription } from "common/src/configuration/urgency.js"
import { DeleteListId, ListResponse } from "common/src/common"
import { useContext, useEffect, useState } from "react"
import { Link, useLocation, useNavigate, useNavigation, useSearchParams } from "react-router-dom"

const formNames = {
    descr: "descr",
    code: "code",
    priority: "priority",
}

export default () => {

    const [searchParams, setSearchParams] = useSearchParams();
    const location = useLocation()
    const navigate = useNavigate()
    let { showBar, authAxio } = useContext(AuthContext)
    let { getLang } = useContext(LanguageContext)

    const ps = location?.state?.pageSize ?? 10
    const pn = location?.state?.pageNumber ?? 0
    const isSameCounter = location?.state?.isSameCounter ?? 0

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState<ListResponse<UrgencyListItem>>({ totalRecords: 0, list: [], page: 0 })
    console.log(searchParams, "setting state");

    const [search, setSearch] = useState({
        code: "",
        descr: "",
        priority: ""
    } as { descr: string, code: string, priority: PriorityType | "" })

    const Call = (sp: URLSearchParams) => {
        let newSearch: UregencyFilterItem = {
            descr: sp.get(formNames.descr) ?? "",
            code: sp.get(formNames.code) ?? "",
            priority: sp.get(formNames.priority) as PriorityType ?? undefined,
            pageNumber: pn,
            pageSize: ps
        };

        authAxio.post(urgencyApiDescription.getListUrgencies.path, newSearch).then(x => {
            setData(x.data)
            setIsLoading(false)
        }).catch((out) => {
        })
    }

    useEffect(() => {
        setSearch({
            descr: searchParams.get(formNames.descr) ?? "",
            code: searchParams.get(formNames.code) ?? "",
            priority: searchParams.get(formNames.priority) as PriorityType ?? "",
        })
        Call(searchParams)
    }, [ pn, ps,searchParams, isSameCounter])


    return (
        <Pagination
            isLoading={isLoading}
            title={getLang(Dictionary.Urgencies)}
            other={<Link to={"new"}><Button variant='contained' fullWidth endIcon={<Add />} >add new</Button></Link>}
        >
            <Box marginTop={1} marginBottom={1}>
                {/* // create a filter for the list */}
                <form onSubmit={(e: any) => {
                    e.preventDefault();

                    let values = {
                        [formNames.descr]: e.target[formNames.descr].value,
                        [formNames.code]: e.target[formNames.code].value,
                        [formNames.priority]: e.target[formNames.priority].value as PriorityType
                    }
                    
                    let newSearch = new URLSearchParams()
                    values[formNames.descr] && newSearch.append(formNames.descr, values[formNames.descr])
                    values[formNames.code] && newSearch.append(formNames.code, values[formNames.code])
                    values[formNames.priority] && newSearch.append(formNames.priority, values[formNames.priority])

                    let isSame = newSearch.toString() == searchParams.toString()
                    console.log(isSame, newSearch.toString(), searchParams.toString(), "search");
                    
                    setIsLoading(true)
                    setSearchParams(newSearch, { state: { pageNumber: 0, pageSize: ps, isSameCounter: (isSameCounter+1)  }, replace: (isSame) })
                }}>
                    <Grid container spacing={2}>

                        {/* FILTER FIELDS */}
                        <Grid item xs={4}>

                            <TextField
                                size='small'
                                name={formNames.descr}
                                value={search.descr}
                                onChange={(e) => setSearch({ ...search, descr: e.target.value })}
                                fullWidth
                                label={getLang(Dictionary.Description)}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                size='small'
                                value={search.code}
                                onChange={(e) => setSearch({ ...search, code: e.target.value })}
                                fullWidth
                                name={formNames.code}
                                label={getLang(Dictionary.Code)}
                            />

                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                select
                                value={search.priority}
                                name={formNames.priority}
                                label={getLang(Dictionary.Priority)}
                                fullWidth
                                onChange={(e) => setSearch({ ...search, priority: e.target.value as PriorityType })}
                                size="small"
                            >
                                <MenuItem value={PriorityType.Low}>Low</MenuItem>
                                <MenuItem value={PriorityType.High}>High</MenuItem>
                                <MenuItem value={PriorityType.Mid}>Mid</MenuItem>
                            </TextField>
                        </Grid>

                        <Grid item xs={6} />
                        <Grid item xs={3}>
                            <Button
                                variant="outlined"
                                fullWidth
                                type='submit'
                                endIcon={<Search />}
                            >
                                {getLang(Dictionary.Search)}
                            </Button>
                        </Grid>
                        <Grid item xs={3}>
                            <Button
                                variant="outlined"
                                fullWidth
                                onClick={() => {
                                    // setSearchParams({})
                                    setSearch({code: "", descr: "", priority: ""})
                                }}
                                endIcon={<Clear />}
                            >
                                {getLang(Dictionary.ClearFilter)}
                            </Button>
                        </Grid>

                    </Grid>
                </form>
            </Box>
            <Grid item xs flexDirection={"column"}>
                <TableContainer component={Paper} sx={{ backgroundColor: colors.grey[300] }}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>

                                <TableCell>{getLang(Dictionary.Code)}</TableCell>
                                <TableCell>{getLang(Dictionary.Description)}</TableCell>
                                <TableCell>{getLang(Dictionary.Color)}</TableCell>
                                <TableCell>{getLang(Dictionary.Priority)}</TableCell>
                                <TableCell align="right">{getLang(Dictionary.Actions)}</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.list.map((item: UrgencyListItem, i) => {
                                return (
                                    <TableRow key={item.id}>
                                        <TableCell>{item.code}</TableCell>
                                        <TableCell>{item.descr}</TableCell>
                                        <TableCell ><Icon sx={{ color: item.color ?? "black" }}><Circle /></Icon></TableCell>
                                        <TableCell>{item.priority}</TableCell>
                                        <TableCell align='right'>

                                            <IconButton onClick={() => { navigate(`edit/${item.id}`) }}><Icon sx={{ color: "blue" }} ><Edit /></Icon></IconButton>
                                            <IconButton onClick={() => {

                                                let value: DeleteListId = {
                                                    ids: [item.id]
                                                }

                                                authAxio.post(urgencyApiDescription.deleteUrgencies.path, value).then(x => {
                                                    navigate("", {
                                                        state: { pageNumber: 0, pageSize: ps },
                                                    })
                                                    setIsLoading(true)
                                                    showBar("Urgency deleted", "success")
                                                }).catch(e => {
                                                    console.log(e);
                                                })
                                            }}>

                                                <Icon ><Delete /></Icon>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 50]}
                    component="div"
                    count={data.totalRecords}
                    rowsPerPage={ps}
                    labelRowsPerPage={getLang(Dictionary.RowsPerPage)}
                    page={pn}
                    onPageChange={(a, page) => {
                        console.log(page);
                        navigate("", {
                            state: { pageNumber: page, pageSize: ps },
                        })
                    }}
                    onRowsPerPageChange={(e) => {
                        navigate("", {
                            state: { pageNumber: 0, pageSize: e.target.value },
                        })
                    }}
                />
            </Grid>
        </Pagination>
    )
}
