import { useContext, useEffect, useState } from "react";
import Pagination from "../Pagination/PaginationBase";
import { Formik } from "formik";
import { Form, useNavigate, useParams } from "react-router-dom";
import { Button, CircularProgress, Grid, MenuItem, Paper, TextField, Typography, colors } from "@mui/material";
import { PriorityType, UrgencyItem, urgencyApiDescription, validateUrgencyItem } from "common/src/configuration/urgency.js";
import { ItemWrapper } from "common/src/common"
import { AuthContext } from "Contexts/Auth";
import { Dictionary, LanguageContext } from "Contexts/Language";
import BaseTextField from "Components/BaseTextField";

const formName = {
    CODE: "code",
    DESCR: "descr",
    COLOR: "color",
    PRIORITY: "priority"
}
export default () => {

    let navigate = useNavigate()
    const { getLang } = useContext(LanguageContext)
    let [isLoading, setIsLoading] = useState(false)
    let { authAxio } = useContext(AuthContext)

    return (<Pagination title="New urgency" isLoading={isLoading}>
        <Formik
            initialValues={{
                code: "",
                descr: "",
                color: "#000000",
                priority: PriorityType.Low
            } as UrgencyItem}
            validationSchema={validateUrgencyItem}
            onSubmit={(values) => {
                console.log(values);

                let data = {
                    item: values
                } as ItemWrapper<UrgencyItem>;

                authAxio.post(`${urgencyApiDescription.postUrgency.path}`, data).then(x => {
                    if (x.status == 200) {
                        console.log("nvaigate back");
                        navigate("./..")
                    }
                    setIsLoading(false)
                })
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => {
                console.log(errors);
                
                return (

                    <Paper sx={{ padding: 2, backgroundColor: colors.grey[200] }} >
                        <Grid container component={Form} method="post" onSubmit={handleSubmit} spacing={3}>
                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.CODE}
                                    label={getLang(Dictionary.Code)} />
                            </Grid>
                            <Grid item xs={6} />
                            <Grid item xs={12}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.DESCR}
                                    label={getLang(Dictionary.Description)} />
                            </Grid>
                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.PRIORITY}
                                    props={{
                                        select: true
                                    }}
                                    label={getLang(Dictionary.Priority)}>
                                    <MenuItem value={PriorityType.High}>High</MenuItem>
                                    <MenuItem value={PriorityType.Mid}>Mid</MenuItem>
                                    <MenuItem value={PriorityType.Low}>Low</MenuItem>
                                    {/* <MenuItem value={""}><em>{getLang(Dictionary.None)}</em></MenuItem> */}
                                </BaseTextField>
                            </Grid>
                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.COLOR}
                                    props={{
                                        type: "color"
                                    }}
                                    label={getLang(Dictionary.Color)} />
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    sx={{ float: "right" }}
                                    type="submit"
                                    variant="contained">
                                    {getLang(Dictionary.Save)}
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                )
            }}</Formik>
    </Pagination>)
}