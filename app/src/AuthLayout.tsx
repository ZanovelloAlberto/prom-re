

import { useTheme } from "@emotion/react";
import { Inbox, LogoutOutlined, Mail, Menu } from "@mui/icons-material";
import { Alert, AppBar, Box, Button, Container, Divider, Drawer, Hidden, Icon, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Stack, Toolbar, Typography } from "@mui/material";
import { AuthContext } from "Contexts/Auth";
import { LanguageSelector } from "Contexts/Language";
import CustomAlert from "CustomAlert";
import SideMenu, { MenuFlat } from "SideMenu";
import Topbar from "Topbar";
import { log } from "console";
import { theme } from "index";
// import {} from ""
// import { AuthLoginResponse } from "../../common/shared-api/login";
import { createContext, useContext, useEffect, useState } from "react";
import { Link, Navigate, Outlet, redirect, useLocation, useNavigate, } from "react-router-dom";

const drawerWidth = 320;
export interface Session {
    token: string
    currentUserId: string
}


export default () => {

    let navigate = useNavigate()
    let {Logout} = useContext(AuthContext)


    const [mobileOpen, setMobileOpen] = useState(false);
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    return (<>
        <AppBar position="fixed"
            sx={{
                [theme.breakpoints.up('md')]: {
                    width: `calc(100% - ${drawerWidth}px)`,
                    marginLeft: `${drawerWidth}px`,
                },
                height: "64px"
            }}
        >

            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    onClick={handleDrawerToggle}
                    sx={{
                        marginRight: theme.spacing(2),
                        [theme.breakpoints.up('md')]: {
                            display: 'none',
                        },
                    }}
                >
                    <Menu />
                </IconButton>
                <Typography variant="h6" noWrap sx={{ flexGrow: 1 }}>
                    Prometeo
                </Typography>
                <Stack direction="row" spacing={2}>
                    
                <LanguageSelector />
                {/* {user == null ? "ciao" : <Typography variant="h6" noWrap >{user.email}</Typography>} */}
                <Button endIcon={<LogoutOutlined/>} onClick={() => {
                    Logout();
                }} color="inherit">Logout</Button>
                </Stack>
            </Toolbar>
        </AppBar>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
            variant={"temporary"}
            anchor={'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            sx={{
                [theme.breakpoints.up('md')]: {
                    display: 'none',
                },
                overflow: "hidden",
                overflowY: "hidden",

            }}
            ModalProps={{
                keepMounted: true, // Better open performance on mobile.
            }}
        >
            <Box role="presentation" sx={{ width: drawerWidth }}>
                <MenuFlat />
            </Box>
        </Drawer>
        <Drawer
            sx={{
                width: drawerWidth,
                [theme.breakpoints.down('md')]: {
                    display: 'none'
                },
                overflow: "hidden"

            }}
            variant="permanent"
            open
        >
            <Box role="presentation" sx={{ width: drawerWidth }}>
                <MenuFlat />
            </Box>
        </Drawer>

        <Box sx={{
            [theme.breakpoints.up('md')]: {
                width: `calc(100% - ${drawerWidth}px)`,
                marginLeft: `${drawerWidth + 20}px`,
            },
            marginTop: "64px",
        }}>
            <Outlet />
        </Box>
    </>)
}

1