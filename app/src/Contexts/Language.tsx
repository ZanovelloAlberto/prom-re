import { MenuItem, TextField } from "@mui/material"
import { createContext, useContext, useState } from "react"

export enum Language {
    Italian = 0,
    English = 1,
    German = 2
}

export const getLang = (values: string[]) => {
    return values[Language.Italian]
}
export const LanguageContext = createContext({} as ReturnType<typeof value>)
const value = () => {
    let lang = localStorage.getItem("language")
    const [language, setLanguage] = useState(lang ? parseInt(lang) : Language.Italian)
    const getLang = (key: string[]) => {
        localStorage.setItem("language", language.toString())
        return key[language]
    }
    return { language, setLanguage, getLang }
}

export const LanguageContextProvider = ({ children }: any) => {
    return (
        <LanguageContext.Provider value={value()}>
            {children}
        </LanguageContext.Provider>
    );
}

export const LanguageSelector = () => {
    let { language, setLanguage } = useContext(LanguageContext)
    return (
        <TextField
            size="small"
            select
            label={getLang(Dictionary.Language)}
            value={language}
            onChange={(e) => {
                setLanguage(parseInt(e.target.value))
            }}>
            <MenuItem value={Language.Italian}>Italiano</MenuItem>
            <MenuItem value={Language.English}>English</MenuItem>
            <MenuItem value={Language.German}>Deutsch</MenuItem>
        </TextField>)
}
export const Dictionary = {
    Read: [
        "Lettura", "Read", "Lesen"
    ],
    ReadWrite: [
        "Lettura Scrittura", "Read Write", "Lesen Schreiben"
    ],
    EmailNotVerified: [
        "Email non verificata", "Email not verified", "E-Mail nicht überprüft"
    ],
    Save: [
        "Salva", "Save", "Speichern"
    ],
    Contacts: [
        "Contatti", "Contacts", "Kontakte"
    ],
    Contact: [
        "Contatto", "Contact", "Kontakt"
    ],
    Deleted: [
        "Eliminato", "Deleted", "Gelöscht"
    ],
    Permissions: [
        "Permessi", "Permissions", "Berechtigungen"
    ],
    Name: [
        "Nome", "Name", "Name"
    ],
    Password: [
        "Password", "Password", "Passwort"
    ],
    EmailVerified: [
        "Email Verificata", "Email Verified", "E-Mail überprüft"
    ],
    Email: [
        "Email", "Email", "Email"
    ],
    User: [
        "Utente", "User", "Benutzer"
    ],
    Users: [
        "Utenti", "Users", "Benutzer"
    ],
    RowsPerPage: [
        "Righe per pagina", "Rows per page", "Zeilen pro Seite"
    ],
    YourContentGoesHere: [
        "Il tuo contenuto va qui", "Your content goes here", "Ihr Inhalt geht hier"
    ],
    Language: [
        "Lingua", "Language", "Sprache"
    ],
    Type: [
        "Tipo", "Type", "Typ"
    ],
    ClearFilter: [
        "Pulisci Filtro", "Clear Filter", "Filter löschen"
    ],
    Added: [
        "Aggiunto", "Added", "Hinzugefügt"
    ],
    Add: [
        "Aggiungi", "Add", "Hinzufügen"
    ],
    New: [
        "Nuovo", "New", "Neu"
    ],
    None: [
        "Nessuno", "None", "Keiner"
    ],
    Search: [
        "Cerca", "Search", "Suche"
    ],
    Urgency: [
        "Urgenza", "Urgency", "Dringlichkeit"
    ],
    Urgencies: [
        "Urgenze", "Urgencies", "Dringlichkeiten"
    ],
    NewUrgency: [
        "Nuova Urgenza", "New Urgency", "Neue Dringlichkeit"
    ],
    Edited: [
        "Modificato", "Edited", "Bearbeitet"
    ],
    Edit: [
        "Modifica", "Edit", "Bearbeiten"
    ],
    EditUrgency: [
        "Modifica Urgenza", "Edit Urgency", "Dringlichkeit bearbeiten"
    ],
    Code: [
        "Codice", "Code", "Code"
    ],
    Description: [
        "Descrizione", "Description", "Beschreibung"
    ],
    Priority: [
        "Priorità", "Priority", "Priorität"
    ],
    Color: [
        "Colore", "Color", "Farbe"
    ],
    Value: [
        "Valore", "Value", "Wert"
    ],
    Actions: [
        "Azioni", "Actions", "Aktionen"
    ],
    SendEmail: [
        "Invia Email", "Send Email", "E-Mail senden"
    ],
    EmailSent: [
        "Email inviata", "Email sent", "E-Mail gesendet"
    ],
    UrgencyAdded: [
        "Urgenza aggiunta", "Urgency added", "Dringlichkeit hinzugefügt"
    ],
}