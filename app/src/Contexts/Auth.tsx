import { Alert } from "@mui/material";
import { baseConfig, publicAxio } from "Api";
import AuthLayout from "AuthLayout";
import axios, { AxiosError, HttpStatusCode } from "axios";
import { createContext, useContext, useEffect, useState } from "react"
import { Navigate } from "react-router-dom";
interface CustomAlertProps {
    enable: boolean
    message: string;
    severity: 'error' | 'warning' | 'info' | 'success';
}
interface User {
    name: string;
    email: string;
    password: string;
}
export interface Session {
    token: string
    currentUserId: string
}

function setCookie(name: string, value: string) {
    const date = new Date();
    date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
    const expires = "expires=" + date.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

export const AuthContext = createContext({} as ReturnType<typeof value>)

const value = () => {
    let newSession = localStorage.getItem('session')
    const [session, setSession] = useState<Session | undefined>(newSession ? JSON.parse(newSession) : undefined)

    const authAxio = axios.create({
        ...baseConfig,
        headers: {
            Authorization: session?.token,
            "Content-Type": "application/json"
        },
        transformRequest: (data, headers) => {
            return JSON.stringify(data);

        },
        transformResponse: (data) => {
            console.log(data);

            return JSON.parse(data);
        }
    })
    const [showBarState, setShowBarState] = useState<CustomAlertProps>({ enable: false, message: "", severity: "info" })
    const showBar = (message: string, severity: 'error' | 'warning' | 'info' | 'success') => {
        setShowBarState({ enable: true, message, severity })
        const timer = setTimeout(() => {
            setShowBarState({ enable: false, message: "", severity: "info" });
        }, 1500);
        return () => clearTimeout(timer);
    }
    // authAxio forwward response only if status is 200


    authAxio.interceptors.response.use((response) => {
        // console.log(response);
        // if (response.status === HttpStatusCode.Accepted) {
        // showBar("Success", 'success')
        // }
        console.log(response.status);

        switch (response.status) {
            case HttpStatusCode.Accepted:
            case HttpStatusCode.Created:
                showBar("Success", 'success')
                break;
            case HttpStatusCode.Ok:
                break;
            default:
                break;
        }

        return response
    }, (error: AxiosError) => {

        console.log(error);
        switch (error.response?.status) {

            case HttpStatusCode.Unauthorized:
                console.log("doing logout");
                Logout()
                break;
            case HttpStatusCode.BadRequest:
                console.log("bad request", error.response.data);
                showBar("aaa", 'error')
                break;
            case HttpStatusCode.InternalServerError:
                showBar("Internal server error", 'error')
                break;
            case HttpStatusCode.NotFound:
                showBar("Not found", 'error')
                break;
            default:
                console.log("unhandled status", error?.response?.status);
                break;
        }
        return error
    })


    const [user, setUser] = useState<User>()



    const Login = async (email: string, password: string) => {
        try {
            let response = await publicAxio.post("/login", { email, password })
            setSession(response.data as Session);
            localStorage.setItem('session', JSON.stringify(response.data));
        } catch (error) {
            return false
        }

        return true
    }
    const Logout = () => {
        setSession(undefined);
        localStorage.removeItem('session');
    }


    return { showBar, showBarState, Login, Logout, user, setUser, authAxio, session }
}



export const AuthContextProvider = ({ children }: any) => {
    return (
        <AuthContext.Provider value={value()}>
            <AlertDialog />
            {children}
        </AuthContext.Provider>
    );
}

export const AuthChecker = () => {
    let { user, authAxio, setUser, session, Logout } = useContext(AuthContext)


    if (!session) {
        return <Navigate to={"login"} />
    }

    useEffect(() => {
        if (session?.currentUserId) {
            authAxio.get(`/users/${session?.currentUserId}`).then(x => {
                setUser(x.data)
            })
        }
    }, [session])

    return <AuthLayout />
    //     let session = isAuth()
    //     useEffect(() => {
    //         if(session?.currentUserId){
    //             authAxio.get(`users/${session?.currentUserId}`).then(x => {
    //                 setUser(x.data)
    //             }).catch((out) => {
    //                 Logout();
    //             })
    //         }
    //     },[session])


    //     return (
    //         <>
    //             {session ? <RouterProvider router={AuthRoutesRouter} /> : <RouterProvider router={PublicRoutesRouter} />}
    //         </>
    //     );
}


const AlertDialog = () => {
    let { showBarState } = useContext(AuthContext)
    return (
        <>
            {showBarState.enable && <div style={{ position: 'fixed', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>
                <Alert severity={showBarState.severity}>{showBarState.message}</Alert>
            </div>}
        </>

    );
}