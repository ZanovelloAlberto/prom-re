import React, { useEffect, useState } from 'react';
import { Alert } from '@mui/material';

interface CustomAlertProps {
    message: string;
    severity: 'error' | 'warning' | 'info' | 'success';
}

export default ({ message, severity }: CustomAlertProps) => {
    let [show, setShow] = useState(false)
    useEffect(() => {
        setShow(true);
        const timer = setTimeout(() => {
            setShow(false);

            // close the alert after 3 seconds
            // console.log('This will run after 3 seconds!');
        }, 1500);
        return () => clearTimeout(timer);
    }, [message, severity]);


    if (show) return (
        <div style={{ position: 'fixed', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>
            <Alert sx={{width : 3}} severity={severity}>{message}</Alert>
        </div>
    );
    return <></>
};