import { useContext, useEffect, useState } from "react";
import Pagination from "../Pagination/PaginationBase";
import { Formik } from "formik";
import { Form, useNavigate, useParams } from "react-router-dom";
import { Button, Grid, MenuItem, Paper, TextField, Typography, colors } from "@mui/material";
import {  UserItem, UserApiDescription, validateUser, DefaultPermissions } from "common/src/configuration/user";
import { ItemWrapper } from "common/src/common";
import { AuthContext } from "Contexts/Auth";
import { Dictionary, LanguageContext } from "Contexts/Language";
import BaseTextField from "Components/BaseTextField";
import { number } from "yup";
enum formName {
    EMAIL = "email",
    PASSWORD = "password",
    NAME = "name",
    DESCR = "descr",
    PERMISSIONS = "permissions"
}
export default () => {

    let { getLang } = useContext(LanguageContext)
    let navigate = useNavigate()
    let { showBar, authAxio } = useContext(AuthContext)

    let params = useParams()
    let [isLoading, setIsLoading] = useState(true)
    let [data, setData] = useState<UserItem>({
        email: "",
        name: "",
        permissions: DefaultPermissions,
        id: "",
        emailVerified: false,

    })
    useEffect(() => {
        authAxio.get(`${UserApiDescription.getSingeUser.path}/${params.id}`).then(x => {

            let data = x.data;

            setData({
                [formName.EMAIL]: data[formName.EMAIL],
                [formName.NAME]: data[formName.NAME],
                [formName.PASSWORD]: "",
                [formName.DESCR]: data[formName.DESCR],
                [formName.PERMISSIONS]: data[formName.PERMISSIONS],

            } as UserItem)
            setIsLoading(false)
        })
    }, [])

    return (<Pagination title={`${getLang(Dictionary.Edit)} ${getLang(Dictionary.User)}`} isLoading={isLoading}>
        <Formik
            enableReinitialize
            initialValues={data}
            validationSchema={validateUser}
            onSubmit={(values) => {
                let data = {
                    id: params.id,
                    item: values
                } as ItemWrapper<UserItem>;
                console.log(values);


                authAxio.post(`${UserApiDescription.postUser.path}`, data).then(x => {
                    navigate("./../..")
                    // showBar(`${getLang(Dictionary.User)} ${getLang(Dictionary.Added)}`, "success")
                })
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => {
                return (
                    <Paper sx={{ padding: 4, backgroundColor: colors.grey[200] }} >
                        <Grid container component={Form} method="post" replace onSubmit={handleSubmit} spacing={3}>

                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.EMAIL}
                                    label={getLang(Dictionary.Email)} />
                            </Grid>
                            <Grid item xs={6} />
                            <Grid item xs={12}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    props={{
                                        type: "password"
                                    }}
                                    name={formName.PASSWORD}
                                    label={getLang(Dictionary.Password)} />
                            </Grid>
                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.NAME}

                                    label={getLang(Dictionary.Name)} />
                            </Grid>
                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.NAME}
                                    label={getLang(Dictionary.Description)} />
                            </Grid>
                            <Grid item xs={6}>
                                <BaseTextField
                                    formik={{ values, errors, handleChange }}
                                    name={formName.PERMISSIONS}
                                    label={getLang(Dictionary.Permissions)}
                                    props={{
                                        select: true,
                                        SelectProps: {
                                            multiple: true,
                                        }
                                    }}
                                >
                                    {/* PermissionValue */}
                                    {/* <MenuItem value={Permioss} */}
                                    {/* {Object.keys(DefaultPermissions).map((x) => (
                                        <MenuItem key={x} value={x}>
                                            {x}
                                        </MenuItem>
                                    ))} */}
                                </BaseTextField>
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    sx={{ float: "right" }}
                                    type="submit"
                                    variant="contained">
                                    submit
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>

                )
            }}</Formik>
    </Pagination>)
}