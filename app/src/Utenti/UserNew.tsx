import { useContext, useEffect, useState } from "react";
import Pagination from "../Pagination/PaginationBase";
import { Formik } from "formik";
import { Form, useNavigate, useParams } from "react-router-dom";
import { Box, Button, CheckboxProps, CircularProgress, Container, Divider, Grid, MenuItem, Paper, TextField, Typography, colors } from "@mui/material";
import { UserItem, UserApiDescription, DefaultPermissions, validateNewUser, UserNewItem, PermissionValue } from "common/src/configuration/user";
import { AuthContext } from "Contexts/Auth";
import { Dictionary, LanguageContext } from "Contexts/Language";
import BaseTextField from "Components/BaseTextField";
import { DeleteListId, HttpStatuses, ItemWrapper } from "common/src/common"

enum formName {
    EMAIL = "email",
    PASSWORD = "password",
    NAME = "name",
    DESCR = "descr",
    PERMISSIONS = "permissions"
}
let initialValues: UserNewItem = {
    email: "",
    password: "",
    descr: "",
    name: "",
    permissions: DefaultPermissions,
}
export default () => {
    let navigate = useNavigate()
    const { getLang } = useContext(LanguageContext)
    let [isLoading, setIsLoading] = useState(false)
    useEffect(() => {
    }, [])
    let { showBar, authAxio } = useContext(AuthContext)

    return (<Pagination title={`${getLang(Dictionary.New)} ${getLang(Dictionary.User)}`} isLoading={isLoading}>
        <Formik
            initialValues={initialValues}
            validationSchema={validateNewUser}
            onSubmit={(values) => {
                let data = {
                    item: values
                } as ItemWrapper<UserItem>;

                authAxio.post(`${UserApiDescription.postUser.path}`, data).then(x => {
                    if (x.status === HttpStatuses.Created) {
                        navigate("./..")
                    }
                    setIsLoading(false)
                    // showBar(`${getLang(Dictionary.User)} ${getLang(Dictionary.Edited)}`, "success")
                })
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => (
                <Paper sx={{ padding: 2, backgroundColor: colors.grey[200] }} >
                    <Grid container component={Form} method="post" replace onSubmit={handleSubmit} spacing={3}>

                        <Grid item xs={6}>
                            <BaseTextField
                                formik={{ values, errors, handleChange }}
                                name={formName.EMAIL}
                                label={getLang(Dictionary.Email)} />
                        </Grid>
                        <Grid item xs={6} />
                        <Grid item xs={12}>
                            <BaseTextField
                                formik={{ values, errors, handleChange }}
                                props={{
                                    type: "password"
                                }}
                                name={formName.PASSWORD}
                                label={getLang(Dictionary.Password)} />
                        </Grid>
                        <Grid item xs={6}>
                            <BaseTextField
                                formik={{ values, errors, handleChange }}
                                name={formName.NAME}
                                label={getLang(Dictionary.Name)} />
                        </Grid>
                        <Grid item xs={6}>
                            <BaseTextField
                                formik={{ values, errors, handleChange }}
                                name={formName.DESCR}
                                label={getLang(Dictionary.Description)} />
                        </Grid>
                        <Grid item xs={12} marginTop={3}>
                            {/* <br/> */}
                            {/* <Divider /> */}
                            <Typography align={"left"} variant="h5">{getLang(Dictionary.Permissions)}</Typography>
                        </Grid>
                        {/* <Grid item xs={6}> */}
                        {
                            Object.keys(DefaultPermissions).map((x) => {
                                return (
                                    <Grid item xs={6}>
                                        <Typography variant="h6">{x}</Typography>
                                        {Object.keys((DefaultPermissions as any)[x]).map((y) => (

                                            <Box marginTop={1} marginBottom={1} key={y}>
                                                {/* <Grid item xs={6}> */}
                                                <Typography> {y}</Typography>
                                                <TextField
                                                    // formik={{ values, errors, handleChange }}
                                                    name={formName.PERMISSIONS}
                                                    // label={y}
                                                    select
                                                    fullWidth
                                                    size="small"
                                                    // type={""}
                                                    onChange={(e: any) => {
                                                        let newPermissions = { ...values.permissions }
                                                        newPermissions[x][y] = e.target.value
                                                        handleChange({ target: { name: formName.PERMISSIONS, value: newPermissions } })
                                                    }}
                                                    value={values.permissions[x][y]}
                                                >
                                                    <MenuItem value={PermissionValue.Read}>{getLang(Dictionary.Read)}</MenuItem>
                                                    <MenuItem value={PermissionValue.ReadWrite}>{getLang(Dictionary.ReadWrite)}</MenuItem>
                                                    <MenuItem value={PermissionValue.None}>{getLang(Dictionary.None)}</MenuItem>
                                                </TextField>
                                                {/* </Grid> */}
                                            </Box>))}

                                    </Grid>

                                )
                            })
                        }
                        {/* </Grid> */}

                        {/* <Grid item xs={6}>
                            <BaseTextField
                                formik={{ values, errors, handleChange }}
                                name={formName.PERMISSIONS}
                                label={getLang(Dictionary.Permissions)}
                                props={{
                                    select: true,
                                    SelectProps: {
                                        multiple: true,
                                    }
                                }}
                            >
                                {Object.keys(DefaultPermissions).map((x) => (
                                    <MenuItem key={x} value={x}>
                                        {x}
                                    </MenuItem>
                                ))}
                            </BaseTextField>
                        </Grid> */}

                        <Grid item xs={12}>
                            <Button
                                sx={{ float: "right" }}
                                type="submit"
                                variant="contained">
                                submit
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
            )}</Formik>
    </Pagination>)
}