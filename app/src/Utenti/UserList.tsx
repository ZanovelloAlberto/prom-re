import { Add, CheckBox, Circle, Clear, Delete, Edit, Label, QueryBuilder, Search } from "@mui/icons-material"
import { TextField, Stack, Grid, Button, TableCell, TableRow, TableBody, TableHead, Paper, colors, TableContainer, Table, TablePagination, Icon, IconButton, Box, Select, MenuItem, Container, Checkbox, FormControlLabel, } from "@mui/material"
import { AuthContext } from "Contexts/Auth"
import { Dictionary, LanguageContext } from "Contexts/Language"
import Pagination from "Pagination/PaginationBase"
import { HttpStatusCode } from "axios"
import { loadavg } from "os"
import { UserItem, UserApiDescription, validateUser, DefaultPermissions, UserListItem, UserFilterItem } from "common/src/configuration/user";
import { DeleteListId, ListResponse } from "common/src/common"
import { useContext, useEffect, useState } from "react"
import { Link, useLocation, useNavigate, useSearchParams } from "react-router-dom"

enum formNames {
    EMAIL = "email",
    NAME = "name",
    VERIFIED = "verified",
}

export default () => {

    const [searchParams, setSearchParams] = useSearchParams();
    const location = useLocation()
    const navigate = useNavigate()
    let { showBar, authAxio } = useContext(AuthContext)
    let { getLang } = useContext(LanguageContext)

    const ps = location?.state?.pageSize ?? 10
    const pn = location?.state?.pageNumber ?? 0
    const isSameCounter = location?.state?.isSameCounter ?? 0

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState<ListResponse<UserListItem>>({ totalRecords: 0, list: [], page: 0 })

    const [search, setSearch] = useState({
        email: "",
        name: "",
        verified: "",
    } as { email: string, name: string, verified: string})

    const Call = (sp: URLSearchParams) => {
        let newSearch: UserFilterItem = {
            email: sp.get(formNames.EMAIL) ?? "",
            name: sp.get(formNames.NAME) ?? "",
            emailVerified: sp.get(formNames.VERIFIED) == "true" ? true : sp.get(formNames.VERIFIED) == "false" ? false : undefined,
            pageNumber: pn,
            pageSize: ps
        };

        authAxio.post(UserApiDescription.getListUsers.path, newSearch).then(x => {
            if (x.status === HttpStatusCode.Ok) {
                setData(x.data)
            }
            setIsLoading(false)
        })
    }

    useEffect(() => {
        setSearch({
            email: searchParams.get(formNames.EMAIL) ?? "",
            name: searchParams.get(formNames.NAME) ?? "",
            verified: searchParams.get(formNames.VERIFIED) ?? "",
        })
        Call(searchParams)
    }, [pn, ps, searchParams, isSameCounter])


    return (
        <Pagination
            isLoading={isLoading}
            title={getLang(Dictionary.Users)}
            other={<Link to={"new"}><Button variant='contained' fullWidth endIcon={<Add />} >add new</Button></Link>}
        >
            <Box marginTop={1} marginBottom={1}>
                {/* // create a filter for the list */}
                <form onSubmit={(e: any) => {
                    e.preventDefault();

                    let values = {
                        [formNames.EMAIL]: e.target[formNames.EMAIL].value,
                        [formNames.NAME]: e.target[formNames.NAME].value,
                        [formNames.VERIFIED]: e.target[formNames.VERIFIED].value,
                    }

                    let newSearch = new URLSearchParams()
                    values[formNames.EMAIL] && newSearch.append(formNames.EMAIL, values[formNames.EMAIL])
                    values[formNames.NAME] && newSearch.append(formNames.NAME, values[formNames.NAME])
                    values[formNames.VERIFIED] && newSearch.append(formNames.VERIFIED, values[formNames.VERIFIED])

                    let isSame = newSearch.toString() == searchParams.toString()

                    setIsLoading(true)
                    setSearchParams(newSearch, { state: { pageNumber: 0, pageSize: ps, isSameCounter: (isSameCounter + 1) }, replace: (isSame) })
                }}>
                    <Grid container spacing={2}>

                        {/* FILTER FIELDS */}
                        <Grid item xs={4}>

                            <TextField
                                size='small'
                                variant="standard"
                                name={formNames.EMAIL}
                                value={search[formNames.EMAIL]}
                                onChange={(e) => setSearch({ ...search, [formNames.EMAIL]: e.target.value })}
                                fullWidth
                                label={getLang(Dictionary.Email)}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                size='small'
                                variant="standard"
                                value={search[formNames.NAME]}
                                onChange={(e) => setSearch({ ...search, [formNames.NAME]: e.target.value })}
                                fullWidth
                                name={formNames.NAME}
                                label={getLang(Dictionary.Name)}
                            />

                        </Grid>
                        <Grid item xs={4}>
                        <TextField
                                size='small'
                                variant="standard"
                                value={search[formNames.VERIFIED]}
                                onChange={(e) => setSearch({ ...search, [formNames.VERIFIED]: e.target.value })}
                                fullWidth
                                select
                                name={formNames.VERIFIED}
                                label={getLang(Dictionary.Name)}
                            >
                                <MenuItem value={"true"}>{getLang(Dictionary.EmailVerified)}</MenuItem>
                                <MenuItem value={"false"}>{getLang(Dictionary.EmailNotVerified)}</MenuItem>
                                <MenuItem value={""}>{getLang(Dictionary.None)}</MenuItem>

                                </TextField>                        </Grid>


                        <Grid item xs={6} />
                        <Grid item xs={3}>
                            <Button
                                variant="outlined"
                                fullWidth
                                type='submit'
                                endIcon={<Search />}
                            >
                                {getLang(Dictionary.Search)}
                            </Button>
                        </Grid>
                        <Grid item xs={3}>
                            <Button
                                variant="outlined"
                                fullWidth
                                onClick={() => {
                                    // setSearchParams({})
                                    setSearch({ [formNames.NAME]: "", [formNames.EMAIL]: "" , verified: ""})
                                }}
                                endIcon={<Clear />}
                            >
                                {getLang(Dictionary.ClearFilter)}
                            </Button>
                        </Grid>

                    </Grid>
                </form>
            </Box>
            <Grid item xs flexDirection={"column"}>
                <TableContainer component={Paper} sx={{ backgroundColor: colors.grey[300] }}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell>{getLang(Dictionary.Email)}</TableCell>
                                <TableCell>{getLang(Dictionary.Name)}</TableCell>
                                <TableCell align="center">{getLang(Dictionary.EmailVerified)}</TableCell>
                                <TableCell align="right">{getLang(Dictionary.Actions)}</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.list.map((item: UserListItem, i) => {
                                return (
                                    <TableRow key={item.id}>
                                        <TableCell>{item.email}</TableCell>
                                        <TableCell>{item.name}</TableCell>
                                        <TableCell align="center"><Checkbox disabled checked={item.emailVerified} /></TableCell>
                                        <TableCell align='right'>

                                            <IconButton onClick={() => { navigate(`edit/${item.id}`) }}><Icon sx={{ color: "blue" }} ><Edit /></Icon></IconButton>
                                            <IconButton onClick={() => {

                                                let value: DeleteListId = {
                                                    ids: [item.id]
                                                }
                                                setIsLoading(true)

                                                authAxio.post(UserApiDescription.deleteUsers.path, value).then(x => {
                                                    if (x.status === HttpStatusCode.Ok) {
                                                        showBar(`${getLang(Dictionary.User)} ${getLang(Dictionary.Deleted)}`, "success")
                                                        navigate("", {
                                                            state: { pageNumber: 0, pageSize: ps },
                                                        })
                                                    }
                                                    setSearchParams(searchParams, { state: { pageNumber: 0, pageSize: ps, isSameCounter: (isSameCounter + 1) }, replace: true })
                                                })
                                            }}>

                                                <Icon ><Delete /></Icon>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 50]}
                    component="div"
                    count={data.totalRecords}
                    rowsPerPage={ps}
                    labelRowsPerPage={getLang(Dictionary.RowsPerPage)}
                    page={pn}
                    onPageChange={(a, page) => {
                        navigate("", {
                            state: { pageNumber: page, pageSize: ps },
                        })
                    }}
                    onRowsPerPageChange={(e) => {
                        navigate("", {
                            state: { pageNumber: 0, pageSize: e.target.value },
                        })
                    }}
                />
            </Grid>
        </Pagination>
    )
}
