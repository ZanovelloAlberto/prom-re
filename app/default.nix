{ pkgs ? import <nixpkgs> { } }: pkgs.stdenv.mkDerivation {


  nativeBuildInputs = with pkgs; [
    nodePackages.pnpm
    #nodejs
    #ngrok

    # mysql80
    postgresql
    #mariadb

    # terraform

  ];

  PROMETEO = "dev";
  
  name = "lll";
  dontUnpack = true;  
  shellHook = ''
  start() {
    pnpm start
  }
'';

}
