{ pkgs ? import <nixpkgs> { }
, pkgsLinux ? import <nixpkgs> { system = "x86_64-linux"; }
}:
let
  server = pkgs.buildGoModule {
    name = "hello";
    src = ./server;
    vendorHash = null;
    tags = [ "netgo" "osusergo" ];
    #       error: buildGoModule: vendorHash is missing
    # ...
  };
in
# pkgs.dockerTools.buildImage {
  #   name = "hello-docker";
  #   config = {
  #     "ExposedPorts"= {  
  #       "8888/tcp"= {};
  #     };
  #     Cmd = [ "${server}/bin/helloworld" ];
  #   };

  # }
pkgs.dockerTools.buildLayeredImage {
  name = "hello-docker";
  fromImage = null;

  # copyToRoot = "${server}/bin/";
  # pkgs.buildEnv {
  #   name = "image-root";
  #   paths = [ pkgs.redis ];
  #   pathsToLink = [ "/bin" ];
  # };
  config = {
    # "ExposedPorts" = {
    #   "8888/tcp" = { };
    # };
    Cmd = [ "${pkgs.bash}/bin/bash -c ${server}/bin/helloworld" ];
  };

}


    
