{ pkgs ? import <nixpkgs> { } }:
let

  # bundle = pkgs.buildNpmPackage {
  bundle = pkgs.stdenv.mkDerivation {
    src = ./.;
    name = "ok";
    # npmDepsHash = "sha256-4ijJK1mkLmK0/YijpjJ+YAux47sf0JFCtXQ7imcayVU=";
    dontUnpack = true;

    installPhase = ''
      mkdir $out
      cp -rL dist node_modules $out
    '';
  };

in
pkgs.dockerTools.buildLayeredImage {
  name = "gcr.io/gcalcli-388911/server-docker-http";
  tag = "latest";
  fromImage = null;
  config = {
    Cmd = [ "${pkgs.nodejs}/bin/node" "${bundle}/dist/app.js" ];
  };
}


    
