interface EmailVerificationTemplateProps {
    verificationLink: string;
    user: {
        name: string;
        email: string;
    };
    companyName: string;
    contactEmail: string;
}
declare const _default: ({ companyName, contactEmail, user, verificationLink }: EmailVerificationTemplateProps) => void;
export default _default;
