import { defaultLocale } from "yup"
import { transporter } from "../../sendMail"

interface EmailVerificationTemplateProps {
    verificationLink: string
    user: {
        name: string
        email: string
    }
    companyName: string
    contactEmail: string

}

export default ({ companyName, contactEmail, user, verificationLink }: EmailVerificationTemplateProps) => {

    let html = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Email Verification</title>
    </head>
    <body style="font-family: Arial, sans-serif;">
    
    <div style="max-width: 600px; margin: 0 auto; padding: 20px; text-align: center;">
        <h2 style="margin-bottom: 20px;">Verify Your Email Address</h2>
        
        <p>Click the button below to verify your email registration as <b>${user.name}</b>:</p>
    
        <a href="${verificationLink}" style="display: inline-block; padding: 15px 30px; background-color: #007bff; color: #fff; text-decoration: none; border-radius: 5px; font-size: 16px; margin-top: 20px;">Verify Email Address</a>
    
        <p style="margin-top: 20px;">If you did not register with <b>${companyName}</b>, please disregard this email.</p>
    
        <p>If you have any questions or concerns, feel free to contact us at <a href="mailto:${contactEmail}" style="color: #007bff; text-decoration: none;">${contactEmail}</a>.</p>
    
        <p>Best regards,<br>
        ${companyName} Team</p>
    </div>
    
    </body>
    </html>
    `
    transporter.sendMail({
        from: contactEmail,
        to: user.email,
        subject: 'Email Verification',
        html,

    }, (error, info) => {
        if (error) {
            console.error('Error occurred:', error);
        } else {
            console.log('Email sent:', info.response);
        }
    })
}