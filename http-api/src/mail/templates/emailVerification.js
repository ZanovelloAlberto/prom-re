"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sendMail_1 = require("../../sendMail");
exports.default = (function (_a) {
    var companyName = _a.companyName, contactEmail = _a.contactEmail, user = _a.user, verificationLink = _a.verificationLink;
    var html = "\n    <!DOCTYPE html>\n    <html lang=\"en\">\n    <head>\n        <meta charset=\"UTF-8\">\n        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n        <title>Email Verification</title>\n    </head>\n    <body style=\"font-family: Arial, sans-serif;\">\n    \n    <div style=\"max-width: 600px; margin: 0 auto; padding: 20px; text-align: center;\">\n        <h2 style=\"margin-bottom: 20px;\">Verify Your Email Address</h2>\n        \n        <p>Click the button below to verify your email registration as <b>".concat(user.name, "</b>:</p>\n    \n        <a href=\"").concat(verificationLink, "\" style=\"display: inline-block; padding: 15px 30px; background-color: #007bff; color: #fff; text-decoration: none; border-radius: 5px; font-size: 16px; margin-top: 20px;\">Verify Email Address</a>\n    \n        <p style=\"margin-top: 20px;\">If you did not register with <b>").concat(companyName, "</b>, please disregard this email.</p>\n    \n        <p>If you have any questions or concerns, feel free to contact us at <a href=\"mailto:").concat(contactEmail, "\" style=\"color: #007bff; text-decoration: none;\">").concat(contactEmail, "</a>.</p>\n    \n        <p>Best regards,<br>\n        ").concat(companyName, " Team</p>\n    </div>\n    \n    </body>\n    </html>\n    ");
    sendMail_1.transporter.sendMail({
        from: contactEmail,
        to: user.email,
        subject: 'Email Verification',
        html: html,
    }, function (error, info) {
        if (error) {
            console.error('Error occurred:', error);
        }
        else {
            console.log('Email sent:', info.response);
        }
    });
});
