import { Response } from 'express'
import { HttpStatuses } from 'common/src/common'
import * as yup from 'yup'

export const validationErrorResponse = (req : Response) => {
    return (err : yup.ValidationError) => {
        req.status(HttpStatuses.BadRequest).json(`validazione server fallita: ${err}`)
    }
}

export const COLLECTION_NAMES = {
    USERS: 'users',
    URGENCIES: 'urgencies',
    CONTACTS: 'contacts',
    COMPANIES: 'companies'
}