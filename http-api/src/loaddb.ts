import { MongoClient } from "mongodb";
import { dbUrgency } from "./configuration/urgency";
import { dbUser } from "./configuration/user";
import { COLLECTION_NAMES } from "./lib";
import { dbContact } from "common/src/configuration/contact";

const uri = "mongodb://root:example@mongo:27017/";
export const client = new MongoClient(uri);
const dbName = 'main';
// if args is passed


const load = async () => {
     
    
    
    await client.db(dbName).collection('users').insertMany([
        { email: "zanovello2002@gmail.com", password: "ciao", name: "alberto", emailVerified: true },
    ] as dbUser[]);

    await client.db(dbName).collection('urgencies').insertMany([
        { code: "SPESA", descr: "fare la spesa", color: "red", priority: "High" }, 
        { code: "NONSO", descr: "ok", color: "yellow", priority: "Low" }, 

  
    ] as dbUrgency[]);

    // await client.db(dbName).collection(COLLECTION_NAMES.COMPANIES).insertMany([
    //     { code: "SPESA", descr: "fare la spesa", color: "red", priority: "High" }, 
    //     { code: "NONSO", descr: "ok", color: "yellow", priority: "Low" }, 
    // ] as dbCompanies[]);

    //contacts
    await client.db(dbName).collection(COLLECTION_NAMES.CONTACTS).insertMany([
        { name: "Luca berdusco", descr: "agente commerciale", },
        { name: "Mattia pellizzari", descr: "Idraulico", },
    ] as dbContact[]);



    console.log("Database loaded");
    client.close()
}
load()


