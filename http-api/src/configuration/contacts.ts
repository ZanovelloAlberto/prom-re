import express, { Application, json } from "express";
import { Document, Filter, MongoClient, ObjectId, WithId } from "mongodb";
import { ObjectShape } from "yup";
import { ContactApiDescription, ContactItem, ContactListItem, dbContact, validateContact, validateContactFilter } from "common/src/configuration/contact";
import { AuthentiateToken } from "../auth/login.js";
import {HttpStatuses, ItemWrapper, ListResponse, validateDeleteList} from "common/src/common";
import { validationErrorResponse } from "../lib.js";


export function initContact(app: Application, dbclient: MongoClient) {
    app.use(json());
    app.use(express.urlencoded({ extended: true }));


    const getCollection = () => {
        const database = dbclient.db('main');
        const Contacts = database.collection<WithId<dbContact>>('Contacts');
        return Contacts;
    }


    app.get(`${ContactApiDescription.getSingeContact.path}/:id`, AuthentiateToken, async (req, res) => {
        let result = await getCollection().findOne({ _id: new ObjectId(req.params.id) });
        if (result == null) {
            res.status(HttpStatuses.NotFound).json("not found");
        } else {
            res.json(result).status(HttpStatuses.Ok);
        }
    });


    app.post(ContactApiDescription.getListContacts.path, AuthentiateToken, async (req, res) => {

        validateContactFilter.validate(req.body).then(async (x) => {

            let coll = getCollection()
            let filter: Filter<Document> = {
                $and: [
                    x.companyId != null ? { companyId: new ObjectId(x.companyId) } : {},
                    x.descr != null ? { descr: { $regex: x.descr, $options: 'i' } } : {},
                    x.name != null ? { name: { $regex: x.name, $options: 'i' } } : {},
                ]
            }
            let pageList = await coll.find(filter).skip(x.pageNumber * x.pageSize).limit(x.pageSize).toArray()

            // coutn the total records
            let totalRecords = await coll.countDocuments(filter);

            res.json({
                list: pageList.map((x) => {
                    return {
                        companyId: x.companyId,
                        id: x._id.toString(),
                        name: x.name,
                        descr: x.descr,
                        values: x.values
                    } as ContactListItem
                }),
                totalRecords,
                page: 0
            } as ListResponse<ContactListItem>)
        }).catch(validationErrorResponse(res));
    });


    app.post(ContactApiDescription.postContact.path, AuthentiateToken, async (req, res) => {

        let data = req.body as ItemWrapper<ContactItem>;

        validateContact.validate(data.item).then(async (x) => {

            try {
                let coll = await getCollection()

                if (data.id != null) {
                    let re = await coll.updateOne({ _id: new ObjectId(data.id) }, { $set: x });
                    res.status(HttpStatuses.Accepted).json(`updated ${re.modifiedCount}`);
                }
                else {
                    let re = await coll.insertOne({
                        ...x,
                        companyId: x.companyId ? new ObjectId(x.companyId) : undefined,
                    } as WithId<dbContact>);
                    res.status(HttpStatuses.Created).json(`inserted ${re.insertedId}`);
                }
            } catch (err) {
                res.status(HttpStatuses.BadRequest).json(`error: ${err}`);
            }

        }).catch(validationErrorResponse(res));

    });


    app.post(ContactApiDescription.deleteContacts.path, AuthentiateToken, async (req, res) => {

        validateDeleteList.validate(req.body).then((x) => {
            getCollection().deleteMany({ _id: { $in: (x.ids.map((x: string) => new ObjectId(x))) } }).then((x) => {
                if (x.acknowledged) {
                    res.status(HttpStatuses.Accepted).json("ok")
                } else {
                    res.status(HttpStatuses.BadRequest).json("error");
                }
            }).catch((err) => {
                // console.log(err);
            })
        }).catch(validationErrorResponse(res));
    });
}








