import express, { Application, json } from "express";
import { MongoClient, ObjectId } from 'mongodb';
import { AuthentiateToken } from "../auth/login.js";
import { transporter } from "../sendMail.js";
import {HttpStatuses} from "common/src/common";
import { notificationAPIDescription } from "common/src/notification/";


export function initNotification(app: Application, dbclient: MongoClient) {

    app.use(json());
    app.use(express.urlencoded({ extended: true }));

    app.post(notificationAPIDescription.notifyMe.path, AuthentiateToken, async (req, res) => {
        console.log("notifyMe");
        
        transporter.sendMail({
            from: 'bo',
            to: 'zanovello2002@gmail.com',
            subject: 'test',
            text: 'test'

        }, (err, info) => {
            if (err) {
                res.json({ error: err }).status(500);
            } else {
                res.json({ info: info }).status(200);
            }
        });

    });
}








