import express, { Application, json } from "express";
import { Document, Filter, MongoClient, ObjectId, WithId } from 'mongodb';
import { AuthentiateToken } from "../auth/login.js";
import {HttpStatuses, FilterItem, ItemWrapper, validateDeleteList, ListResponse, } from "common/src/common";
import { PriorityType, UrgencyItem, UrgencyListItem, urgencyApiDescription, validateUrgencyFilter, validateUrgencyItem } from "common/src/configuration/urgency.js";
import { validationErrorResponse } from "../lib.js";

export interface dbUrgency {
    code: string,
    descr?: string,
    color?: string,
    priority?: PriorityType,
}

export function initUrgency(app: Application, dbclient: MongoClient) {

    app.use(json());
    app.use(express.urlencoded({ extended: true }));

    const getCollection = () => {
        const database = dbclient.db('main');
        const urgencies = database.collection<dbUrgency>('urgencies');
        return urgencies;
    }

    app.get(`${urgencyApiDescription.getSingeUrgency.path}/:id`, AuthentiateToken, async (req, res) => {
        let result = await getCollection().findOne({ _id: new ObjectId(req.params.id) });
        res.json(result)
    });


    app.post(urgencyApiDescription.getListUrgencies.path, AuthentiateToken, async (req, res) => {

        
        validateUrgencyFilter.validate(req.body).then(async (x) => {
            
            let coll = getCollection()
            let filter: Filter<dbUrgency> = {
                $and: [
                    x.code != null ? { code: { $regex: x.code, $options: 'i' } } : {},
                    x.descr != null ? { descr: { $regex: x.descr, $options: 'i' } } : {},
                    x.priority ? { priority: x.priority } : {}
                ]
            }

            let pageList = await coll.find<WithId<dbUrgency>>(filter)
            .skip(x.pageNumber * x.pageSize)
            .limit(x.pageSize)
            // .sort({ code: 1 })
            .toArray()

            // coutn the total records
            let totalRecords = await coll.countDocuments(filter);

            res.json({
                list: pageList.map((x) => {
                    return {
                        id: x._id.toString(),
                        code: x.code,
                        descr: x.descr,
                        color: x.color,
                        priority: x.priority,
                    } as UrgencyListItem
                }),
                totalRecords,
                page: 0
            } as ListResponse<UrgencyListItem>)

        }).catch(validationErrorResponse(res));
    });


    app.post(urgencyApiDescription.postUrgency.path, AuthentiateToken, async (req, res) => {

        let data = req.body as ItemWrapper<UrgencyItem>;

        validateUrgencyItem.validate(data.item).then((x) => {

            if (data.id != null) {
                getCollection().updateOne({ _id: new ObjectId(data.id) }, { $set: x });
            }
            else {
                getCollection().insertOne(x);
            }
            res.status(HttpStatuses.Ok).json("ok");

        }).catch(validationErrorResponse(res));

    });


    app.post(urgencyApiDescription.deleteUrgencies.path, AuthentiateToken, async (req, res) => {

        validateDeleteList.validate(req.body).then((x) => {
            getCollection().deleteMany({ _id: { $in: (x.ids.map((x: string) => new ObjectId(x))) } }).then((x) => {
                if (x.acknowledged) {
                    res.status(HttpStatuses.Ok).json("ok")
                } else {
                    res.status(HttpStatuses.BadRequest).json("error");
                }
            }).catch((err) => {
                console.log(err);
            })
        }).catch(validationErrorResponse(res));
    });
}








