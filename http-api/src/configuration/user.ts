import { randomBytes } from "crypto";
import express, { Application, json } from "express";
import { Document, Filter, MongoClient, ObjectId, WithId } from 'mongodb';
import { AuthentiateToken } from "../auth/login.js";
import { HttpStatuses, ItemWrapper, ListResponse, WithDataInfo, validateDeleteList } from "common/src/common";
import { Permissions, UserItem, UserListItem, UserApiDescription, validateUser, validateUserFilter } from "common/src/configuration/user";
import { validationErrorResponse } from "../lib.js";
import { addMailToPending } from "../auth/mailVerification.js";

export interface dbUser {
    name: string,
    password: string,
    email: string,
    descr?: string,
    permissions: Permissions,
    emailVerified: boolean,
}


export function inituser(app: Application, dbclient: MongoClient) {
    app.use(json());
    app.use(express.urlencoded({ extended: true }));


    const getCollection = () => {
        const database = dbclient.db('main');
        const users = database.collection<WithDataInfo<dbUser>>('users');
        return users;
    }


    app.get(`${UserApiDescription.getSingeUser.path}/:id`, AuthentiateToken, async (req, res) => {
        let result = await getCollection().findOne({ _id: new ObjectId(req.params.id) });
        if (result == null) {
            res.status(HttpStatuses.NotFound).json("not found");
        } else {
            res.json(result).status(HttpStatuses.Ok);
        }
    });


    app.post(UserApiDescription.getListUsers.path, AuthentiateToken, async (req, res) => {

        validateUserFilter.validate(req.body).then(async (x) => {
            // console.log(x);

            let coll = getCollection()
            let filter: Filter<Document> = {
                $and: [
                    x.email != null ? { email: { $regex: x.email, $options: 'i' } } : {},
                    x.name != null ? { name: { $regex: x.name, $options: 'i' } } : {},
                    x.emailVerified != undefined ? { emailVerified: x.emailVerified } : {},
                    ],
            }
            let pageList = await coll.find(filter).skip(x.pageNumber * x.pageSize).limit(x.pageSize).toArray()

            // coutn the total records
            let totalRecords = await coll.countDocuments(filter);

            res.json({
                list: pageList.map((x) => {
                    return {
                        id: x._id.toString(),
                        email: x.email,
                        name: x.name,
                        emailVerified: x.emailVerified,
                    } as UserListItem
                }),
                totalRecords,
                page: 0
            } as ListResponse<UserListItem>)
        }).catch(validationErrorResponse(res));
    });


    app.post(UserApiDescription.postUser.path, AuthentiateToken, async (req, res) => {

        let data = req.body as ItemWrapper<UserItem>;
        let user : WithId<dbUser> = (req as any).user

        validateUser.validate(data.item).then(async (x) => {

            try {
                let coll = await getCollection()

                // check for email duplication
                let emailCheck = await coll.findOne({ email: x.email });
                if (emailCheck != null && emailCheck._id.toString() != data.id) {
                    res.status(HttpStatuses.BadRequest).json("email already exists");
                    return;
                }

                if (data.id != null) {
                    let re = await coll.updateOne({ _id: new ObjectId(data.id) }, { $set: { ...x, lastUpdate: {
                        byUserId: user._id.toString(),
                        at: new Date()
                    } } });
                    res.status(HttpStatuses.Accepted).json(`updated ${re.modifiedCount}`);

                } else {
                    let re = await coll.insertOne({
                        ...x,
                        created: {
                            byUserId: user._id.toString(),
                            at: new Date()
                        },
                        permissions: {},
                        emailVerified: false,
                        password: x.password || ''
                    });
                    res.status(HttpStatuses.Created).json(`inserted ${re.insertedId}`);
                    addMailToPending(x.email, re.insertedId.toString(), x.name);
                }
            } catch (err) {
                res.status(HttpStatuses.BadRequest).json(`error: ${err}`);
            }

        }).catch(validationErrorResponse(res));

    });


    app.post(UserApiDescription.deleteUsers.path, AuthentiateToken, async (req, res) => {

        validateDeleteList.validate(req.body).then((x) => {
            getCollection().deleteMany({ _id: { $in: (x.ids.map((x: string) => new ObjectId(x))) } }).then((x) => {
                if (x.acknowledged) {
                    res.status(HttpStatuses.Accepted).json("ok")
                } else {
                    res.status(HttpStatuses.BadRequest).json("error");
                }
            }).catch((err) => {
                // console.log(err);
            })
        }).catch(validationErrorResponse(res));
    });
}














