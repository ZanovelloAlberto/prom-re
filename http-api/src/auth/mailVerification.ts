import { Application } from "express";
import { randomBytes } from "crypto";
import { MongoClient, WithId } from "mongodb";
import { dbUser } from "../configuration/user";
import emailVerification from "../mail/templates/emailVerification";

const emailVerificationPath = "/auth/emailVerification";
var pendingUsersVerification: { id: string, email: string, verificationCode: string }[] = [];

export const addMailToPending = (email: string, id: string, name: string) => {
    let verificationCode = randomBytes(16).toString('hex');
    emailVerification({
        companyName: "Prometeo",
        contactEmail: "uno@gmail.com",
        user: {
            name,
            email,
        },
        verificationLink: `http://localhost:5000${emailVerificationPath}/${verificationCode}`
    })
    pendingUsersVerification.push({ email, id, verificationCode });
    return verificationCode;
}

export function initEmailVerification(app: Application, dbclient: MongoClient) {

    // app.use(json());
    // app.use(express.urlencoded({ extended: true }));
    const getCollection = () => {
        const database = dbclient.db('main');
        const users = database.collection<WithId<dbUser>>('users');
        return users;
    }

    app.get(`${emailVerificationPath}/:code`, async (req, res) => {
        let code = req.params.code;
        if (code == null) {
            res.status(404).send("not found");
            return;
        }
        let user = pendingUsersVerification.find(x => x.verificationCode == code);
        if (user == null) {
            res.status(404).send("no user found");
            return;
        }
        let coll = getCollection();
        coll.updateOne({ id: user.id }, { $set: { emailVerified: true } }).then(() => {
            res.send(emailVerificationHtmlResponse);
        }).catch((err) => {
            res.status(500).send("error " + err);
        })

    });

}
const emailVerificationHtmlResponse = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email Verification Confirmation</title>
</head>
<body style="font-family: Arial, sans-serif;">

<div style="max-width: 600px; margin: 0 auto; padding: 20px; text-align: center;">
    <h2>Email Verification Successful</h2>
    
    <p>Your email address has been successfully verified. Thank you for completing the verification process.</p>

    <p>You can now access all the features of your account.</p>

</div>

</body>
</html>`


