import express, { Application, NextFunction, Request, Response, json } from "express";
import { MongoClient, WithId } from "mongodb";
import { randomBytes, pbkdf2Sync } from 'crypto';

import { dbUser } from "../configuration/user";
import { validationErrorResponse } from "../lib";
import {AuthLogin, AuthLoginResponse, AuthLoginValidation} from "common/src/login";
import {HttpStatuses} from "common/src/common";

interface SessionToken {
    user: dbUser | null;
    token: string,
}
var sessionToken: SessionToken[] = [{
    token: "123",
    user: {
        email: "emial",
        password: "password",
        name: "name"
    }
} as SessionToken];



function hashPassword(password: string, username: string, email: string): string {
    // Generate a random salt
    // Hash the password using PBKDF2 (Password-Based Key Derivation Function 2)
    const hash = pbkdf2Sync(password, username + email, 100000, 64, 'sha512').toString('hex');
    return hash;
}

function getUserFromToken(token: string): dbUser | null {
    let session = sessionToken.find(x => x.token == token);
    if (session == null) {
        return null;
    }
    return session.user;

}
export function AuthentiateToken(req: Request, res: Response, next: NextFunction) {
        
    if (req.headers.authorization) {
        
        let token = req.headers.authorization;
        
        let user = getUserFromToken(token);
        if (user) {
            console.log(req.body);
            
            (req as any).user = user;
            next();
            return;
        }
    }
    res.status(HttpStatuses.Unauthorized).json("Unauthorized");
}


export function initLogin(app: Application, dbclient: MongoClient) {
    const getCollection = () => {
        const database = dbclient.db('main');
        const urgencies = database.collection<WithId<dbUser>>('users');
        return urgencies;
    }
    
    app.use(json());
    app.use(express.urlencoded({ extended: true }));

    app.post('/login', async (req, res) => {
        console.log(req.body);
        

        
        AuthLoginValidation.validate(req.body).then(async (x ) => {
            let userDb = await getCollection().findOne({ email: x.email })
            if (userDb == null) {
                res.status(HttpStatuses.NotFound).json("user not found");
                return;
            }
            if (userDb.password == x.password) {
                // let token = jwt.sign({ name: userDb.username }, "secret", { expiresIn })
                let token = randomBytes(16).toString('hex');
                sessionToken.push({ token: token, user: userDb });
                res.status(200).json({token, currentUserId: userDb._id.toString()} as AuthLoginResponse);
                return;
            }
            res.status(HttpStatuses.NotFound).json("wrong password");
            return
        }).catch(validationErrorResponse(res));

    });

    // app.post("/newUser", async (req, res) => {
    //     validateLogin.validate(req.body).then(async (x) => {
    //         let userDb = await getCollection().findOne({ username: x.username })
    //         if (userDb != null) {
    //             res.status(401).send("user already exists");
    //             return;
    //         }

    //         getCollection().insertOne(x);
    //         res.status(200).send("ok");

    //     }).catch(err => {
    //         return err.errors
    //     });
    // });
}