// import express, { json, Application } from 'express';
import { MongoClient } from 'mongodb';
import { initUrgency } from './configuration/urgency';
import {  initLogin } from './auth/login';
import { inituser } from './configuration/user';
import { initNotification } from './configuration/nofication';
import cors from 'cors';
import { initEmailVerification } from './auth/mailVerification';
import { initContact } from './configuration/contacts';
import express, { Application, json } from 'express';
export const app : Application = express();
const port = process.env.PORT || 5000;


const uri = "mongodb://root:example@mongo:27017/";
export const client = new MongoClient(uri);

app.use(json());
app.use(cors())

initUrgency(app, client);
initLogin(app, client)
inituser(app, client);
initNotification(app, client);
initEmailVerification(app, client)
initContact(app, client)

app.use(express.static('../app/build'));

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});