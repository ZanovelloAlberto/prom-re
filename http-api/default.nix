{ pkgs ? import <nixpkgs> { } }:
let 

in
pkgs.stdenv.mkDerivation {
  name = "caio";
  src = ./.;
  nativeBuildInputs = with pkgs;[
    nodejs
    # esbuild
    nodePackages.pnpm
    nodePackages.nodemon
    nodePackages.typescript
  ];
  PROMETEO="dev";

  shellHook = ''
    runProd(){
      node dist/loaddb.js;
      while true
      do
        pnpm run start
        # echo cioa
        sleep 1;
      done
    }
    build () {
      esbuild --config=esbuild.config.js
    }
    start () {
      node dist/loaddb.js;
      nodemon --exec "pnpm run start" -w src/ -e "ts"
    }
    '';

}
