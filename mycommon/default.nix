{ pkgs ? import <nixpkgs> { } }:
let 

in
pkgs.stdenv.mkDerivation {
  name = "caio";
  nativeBuildInputs = with pkgs;[
    nodejs
    nodePackages.pnpm
    nodePackages.typescript
    nodePackages.nodemon
  ];
  dontUnpack = true;

  shellHook = ''
    build () {
      esbuild --config=esbuild.config.js
    }
    start () {
      nodemon --exec "tsc" -w src/ -e "ts"
    }
    '';

}
