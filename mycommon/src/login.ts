import * as yup from 'yup';

export const AuthLoginValidation : yup.ObjectSchema<AuthLogin> = yup.object({
    email: yup.string().email().required(),
    password: yup.string().required(),
})

export interface AuthLogin {
    email: string,
    password: string
}
export interface AuthLoginResponse {
    token: string;
    currentUserId: string;
}