import { FilterItem, FilterValidateFields } from "../common"
import * as yup from "yup"

const ContactTypes = {
    mail: 0,
    thelephone: 1,
    mobile: 2,
    fax: 3,
    other: 4,
}
//
enum ContactTypesEnum {
    mail = 0,
    thelephone = 1,
    mobile = 2,
    fax = 3,
    other = 4,
}
export interface ContactValue {
    type: ContactTypesEnum
    value: string
}

export interface dbContact {
    name: string,
    descr?: string,
    values: ContactValue[],
    companyId?: string,
}


//  GET LIST RETURN
export interface ContactListItem {
    id: string
    name: string,
    descr?: string,
    values: ContactValue[],
    companyId: string,
}

// DETAIL RETURN
// NEW INPUT
// EDIT INPUT
export interface ContactItem {
    id: string
    name: string,
    descr?: string,
    values: ContactValue[],
    companyId?: string,
}

// LIST FILTER
export interface ContactFilterItem extends FilterItem {
    name?: string
    descr?: string,
    companyId?: string,
}


export const validateContact = yup.object({
    descr: yup.string().max(50),
    name: yup.string().max(50).required(),
    values: yup.array().of(yup.object({
        type: yup.number().oneOf(Object.values(ContactTypes).map(x => x)).required(),
        value: yup.string().required(),
    })),
    companyId: yup.string().max(50),
}).noUnknown();

export const validateContactFilter: yup.Schema<ContactFilterItem> = yup.object({
    name: yup.string().max(50),
    companyId: yup.string().max(50),
    ...FilterValidateFields
}).noUnknown();

export const ContactApiDescription = {
    getListContacts: {
        path: "/Contacts",
        method: "POST",
    },
    getSingeContact: {
        path: "/Contacts",
        method: "GET",
        params: {
            id: "string",
        },
    },
    postContact: {
        path: "/Contacts/edit",
        method: "POST",
    },
    deleteContacts: {
        path: "/Contacts/delete",
        method: "POST",
        body: {
            listId: "string[]"
        }
    }

}
