import * as yup from "yup"
import { FilterItem, FilterValidateFields } from "../common"


export enum PermissionValue {
    None = 0,
    Read,
    ReadWrite,
}
export interface Permissions {
    [key: string]: {
        [b: string]: PermissionValue

    },
}

export const DefaultPermissions: Permissions = {
    configure: {
        Users: PermissionValue.Read,
        Contacts: PermissionValue.Read,
        Urgencies: PermissionValue.Read,
    },
    activities: {
        Plan: PermissionValue.Read,
        Execute: PermissionValue.Read,
    }
}


//  GET LIST RETURN
export interface UserListItem {
    id: string
    name: string
    email: string
    emailVerified: boolean,
}

// DETAIL RETURN
// EDIT INPUT
export interface UserItem {
    id: string
    name: string
    email: string
    password?: string
    permissions: Permissions
    descr?: string
    emailVerified: boolean,
}

// NEW INPUT
export interface UserNewItem {
    name: string
    email: string
    password: string
    permissions: Permissions
    descr: string
}

// LIST FILTER
export interface UserFilterItem extends FilterItem {
    name?: string
    email?: string
    emailVerified?: boolean,
}


export const validateUser = yup.object({
    descr: yup.string().max(50),
    name: yup.string().max(50).required(),
    email: yup.string().email().required(),
    password: yup.string()
}).noUnknown();

export const validateUserFilter: yup.Schema<UserFilterItem> = yup.object({
    name: yup.string().max(20),
    email: yup.string().max(20),
    emailVerified: yup.boolean(),
    ...FilterValidateFields
}).noUnknown();

export const validateNewUser = yup.object({
    name: yup.string().max(50).required(),
    descr: yup.string().max(50),
    email: yup.string().email().required(),
    password: yup.string().required(),
}).noUnknown();


export const UserApiDescription = {
    getListUsers: {
        path: "/users",
        method: "POST",
    },
    getSingeUser: {
        path: "/users",
        method: "GET",
        params: {
            id: "string",
        },
    },
    postUser: {
        path: "/users/edit",
        method: "POST",
        description: "Create/Update a new User (if id is null is new, otherwise is update)",
    },
    deleteUsers: {
        path: "/Users/delete",
        method: "POST",
        body: {
            listId: "string[]"
        }
    }

}
