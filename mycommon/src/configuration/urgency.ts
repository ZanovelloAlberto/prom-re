import * as yup from "yup"
import { FilterItem, FilterValidateFields } from "../common"

export enum PriorityType {
    High = "High",
    Low = "Low",
    Mid = "Mid",
}

export interface UrgencyListItem {
    id: string
    code: string
    descr?: string
    priority: PriorityType
    color?: string
}

export interface UrgencyItem {
    code: string
    descr?: string
    priority: PriorityType
    color?: string
}

export interface UregencyFilterItem extends FilterItem {
    code?: string
    descr?: string
    priority?: PriorityType
}


export const urgencyApiDescription = {
    getListUrgencies : {
        path : "/urgencies",
        method : "GET",
        description : "Get a list of urgencies",
        body: {
            pageNumber : "number",
            pageSize : "number",
            code : "string",
            description : "string",
        }
    },
    getSingeUrgency : {
        path : "/urgencies",
        method : "GET",
        description : "Get a single urgency",
        params : {
            id : "string",
        },
    },
    postUrgency : {
        path : "/urgency/edit",
        method : "POST",
        description : "Create/Update a new urgency",
        body : {
            id : "string/nullable",
            code : "string",
            descr : "string",
            value : "number",
            color : "string",
        },
    },
    deleteUrgencies : {
        path : "/urgencies/delete",
        method : "POST",
        description : "Delete a list of urgencies",
        body : {
            listId : "string[]"
        }
    }

}


export const validateUrgencyItem : yup.ObjectSchema<UrgencyItem> = yup.object({
    code: yup.string().max(50).required(),
    descr: yup.string().max(255),
    priority: yup.string().oneOf(Object.values(PriorityType)).required(),
    color: yup.string().max(7),
}).noUnknown();

export const validateUrgencyFilter: yup.ObjectSchema<UregencyFilterItem> = yup.object({
    ...FilterValidateFields,
    code: yup.string().max(50),
    descr: yup.string().max(255),
    priority: yup.string().oneOf([...Object.values(PriorityType)]),
}).noUnknown();