export const notificationAPIDescription = {
    notifyMe : {
        path : "/notification/notify",
        method : "POST",
        description : "Get a list of urgencies",
        body: {
            pageNumber : "number",
            pageSize : "number",
            code : "string",
            description : "string",
        }
    },
}