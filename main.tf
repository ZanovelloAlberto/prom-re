terraform {
  backend "local" {
    path = ".terraform/state"
  }
    required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "~> 3.0.1"
    }
  }
}

provider "docker" {}
resource "docker_volume" "shared_volume" {
  name = "shared_volume"
} 



resource "docker_image" "mongodb" {
  name         = "mongo:latest"
  keep_locally = true
}

resource "docker_container" "cmongodb" {
  image = docker_image.mongodb.image_id
  name  = "tutorial"
  env = [
          "MONGO_INITDB_ROOT_USERNAME=root",
      "MONGO_INITDB_ROOT_PASSWORD=example",
    # "ME_CONFIG_MONGODB_URL=mongodb://root:example@mongo:27017/"
  ]
  ports {
    internal = 27017
    external = 27017
  } 
  volumes {
    container_path = docker_volume.shared_volume.mountpoint  
  }
}


output "mongodb_mountpoint" {
  value = docker_volume.shared_volume.mountpoint
}


# locals: Defines local values that can be reused within a Terraform configuration. 
