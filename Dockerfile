FROM nixos/nix:latest AS base

RUN nix-channel --update
RUN nix-env -iA nixpkgs.nodejs
RUN nix-env -iA nixpkgs.nodePackages.pnpm
# RUN nix-env -iA nixpkgs.nodePackages.corepack
RUN nix-env -iA nixpkgs.typescript
COPY . .
# COPY ../mycommon .
# COPY http-api http-akpi/

ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable
WORKDIR /http-api

FROM base AS prod-deps
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --prod 

FROM base AS build
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install
RUN pnpm run build

FROM base
COPY --from=prod-deps /http-api/node_modules /http-api/node_modules
COPY --from=build /http-api/dist /http-api/dist
EXPOSE 8000
CMD [ "pnpm", "start" ]